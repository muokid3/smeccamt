<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Entity extends Model
{
    public function industry()
    {
        return $this->belongsTo('App\Industry', 'industry_id');
    }

    public function type()
    {
        return $this->belongsTo('App\BusinessType', 'business_type_id');
    }

    public function total_trades($entity_id)
    {
        return \App\Bid::where('bidder_entity_id', '=', $entity_id)->where('status','=', 2)->count();

    }
    public function total_donations($entity_id)
    {
        return \App\Donation::where('donor_entity_id', '=', $entity_id)->where('status','=', 2)->count();

    }
}
