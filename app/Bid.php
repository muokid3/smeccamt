<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bid extends Model
{
    public function bidder()
    {
        return $this->belongsTo('App\Entity', 'bidder_entity_id');
    }

    public function payment_mode()
    {
        return $this->belongsTo('App\PaymentMode', 'mode_id');
    }

    public function purchase_order()
    {
        return $this->belongsTo('App\PurchaseOrder', 'purchase_order_id');
    }

    public function bid_status()
    {

        switch ($this->status) {
            case 1:
                return "Pending";
                break;
            case 2:
                return "Approved";
                break;
            case 3:
                return "Not Successful";
                break;
            default:
                return "Not Successful";

        }
    }
}
