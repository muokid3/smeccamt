<?php

function checkActiveRoute($routes, $return_true = false)
{
    return in_array(Request::path(), (array)$routes) ? ($return_true ? true : 'active') : false;
}

if (!function_exists('code')) {
    /**
     * Replaced for UI library code function
     * for each access in phpstorm
     *
     * @param $value
     * @return string
     */
    function code($value)
    {
        $multiplier = rand(10, 99);
        return ($value * $multiplier) . $multiplier;
    }
}

if (!function_exists('decode')) {
    /**
     * Replaced for UI library decode function
     * for each access in phpstorm
     *
     * @param $value
     * @return float|int
     */
    function decode($value)
    {
        return substr($value, 0, strlen($value) - 2) / substr($value, -2);
    }
}