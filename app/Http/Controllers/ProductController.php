<?php

namespace App\Http\Controllers;

use App\Bid;
use App\Donation;
use App\DonorRequest;
use App\Product;
use App\PurchaseOrder;
use App\Stock;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{

    protected $bid;
    protected $donation;


    public function __construct()
    {
        $this->middleware('auth');
        $this->bid = new Bid();
        $this->donation = new Donation();

    }



    public function create(Request $request)
    {

        $this->validate($request, [
            'product_name' => 'required',
            'product_code' => 'required',
        ]);


        if (Auth::user()->entity_id) {
            DB::transaction(function () {

                $request = (object)$_POST;

                $product = new Product();
                $product->product_name = $request->product_name;
                $product->product_code = $request->product_code;
                $product->entity_id = Auth::user()->entity_id;
                if ($product->saveOrFail()){
                    Session::flash('success', 'Product Created Successfully!');
                }else{
                    Session::flash('error', 'An error occurred when creating the product.Please try again');
                }
            });

            return redirect('/profile/products');
        }else{
            Session::flash('error', 'Please complete your profile to continue');
            return redirect('/profile/');
        }

    }


    public function edit(Request $request)
    {

        $this->validate($request, [
            'product_name' => 'required',
            'product_code' => 'required',
        ]);


        if (Auth::user()->entity_id) {
            DB::transaction(function () {

                $request = (object)$_POST;

                $product = Product::find($request->product_id);
                $product->product_name = $request->product_name;
                $product->product_code = $request->product_code;
                if ($product->update()){
                    Session::flash('success', 'Product updated Successfully!');
                }else{
                    Session::flash('error', 'An error occurred when updating the product.Please try again');
                }
            });

            return redirect()->back();
        }else{
            Session::flash('error', 'Please complete your profile to continue');
            return redirect('/profile/');
        }

    }

    public function add_stock(Request $request)
    {

        $this->validate($request, [
            'product' => 'required',
            'unit_size' => 'required',
            'units' => 'required',
        ]);


        if (Auth::user()->entity_id) {
            DB::transaction(function () {

                $request = (object)$_POST;

                $stock = new Stock();
                $stock->product_id = $request->product;
                $stock->entity_id = Auth::user()->entity_id;
                $stock->unit_size = $request->unit_size;
                $stock->units = $request->units;
                if ($stock->saveOrFail()){
                    Session::flash('success', 'Stock Added Successfully!');
                }else{
                    Session::flash('error', 'An error occurred when adding stock.Please try again');
                }
            });

            return redirect('/profile/stock');
        }else{
            Session::flash('error', 'Please complete your profile to continue');
            return redirect('/profile/');
        }

    }

    public function create_purchase_order(Request $request)
    {

        $this->validate($request, [
            'lob' => 'required',
            'product' => 'required',
            'location' => 'required',
            'unit_size' => 'required',
            'units' => 'required',
            'terms' => 'required',
        ]);

        if (Auth::user()->entity_id) {
            DB::transaction(function () {

                $request = (object)$_POST;

                $purchaseOrder = new PurchaseOrder();
                $purchaseOrder->lob_id = $request->lob;
                $purchaseOrder->product_id = $request->product;
                $purchaseOrder->location = $request->location;
                $purchaseOrder->latitude = $request->lat;
                $purchaseOrder->longitude = $request->lng;
                $purchaseOrder->entity_id = Auth::user()->entity_id;
                $purchaseOrder->unit_size = $request->unit_size;
                $purchaseOrder->units = $request->units;
                $purchaseOrder->delivery = $request->delivery;
                $purchaseOrder->terms = $request->terms;
                if ($purchaseOrder->saveOrFail()){
                    Session::flash('success', 'Purchase order created Successfully!');
                }else{
                    Session::flash('error', 'An error occurred when creating the purchase order.Please try again');
                }
            });

            return redirect('/profile/purchase_orders');
        }else{
            Session::flash('error', 'Please complete your profile to continue');
            return redirect('/profile/');
        }

    }

    public function find_orders()
    {
        if (Auth::user()->entity_id) {
            $view = 'find_orders';

            $orders= PurchaseOrder::select('purchase_orders.*')
                ->join('bids','purchase_orders.id', 'bids.purchase_order_id')
                ->where('purchase_orders.entity_id', '!=', Auth::user()->entity_id)
                ->where('bids.bidder_entity_id', '!=', Auth::user()->entity_id)
                ->where('purchase_orders.status', '=', 1)
                ->orderBy('purchase_orders.id', 'desc')
                ->paginate(15);

            return view('user.index')->withOrders($orders)->withView($view);
        }else{
            Session::flash('error', 'Please complete your profile to continue');
            return redirect('/profile/');
        }

    }

    function order_details($order_id)
    {
        $view = 'purchase_orders_details';
        $order = PurchaseOrder::find($order_id);
        if (is_null($order)){
            return view('errors.nothing');
        }else{
            return view('user.index')->withOrder($order)->withView($view);
        }

    }

    public function place_bid(Request $request)
    {

        $this->validate($request, [
            'units' => 'required',
            'price' => 'required',
            'dispatch_date' => 'required',
            'mode' => 'required',
        ]);


        if (Auth::user()->entity_id) {
            DB::transaction(function () {

                $request = (object)$_POST;

                if (Bid::where('purchase_order_id','=', $request->purchase_order_id)->where('bidder_entity_id','=',Auth::user()->entity_id)->first()){
                    Session::flash('error', 'You already placed a bid for this Purchase Order. Please wait for the client to respond');
                }else{
                    $bid = new Bid();
                    $bid->purchase_order_id = $request->purchase_order_id;
                    $bid->bidder_entity_id = Auth::user()->entity_id;
                    $bid->mode_id = $request->mode;
                    $bid->units = $request->units;
                    $bid->unit_price = $request->price;
                    $bid->total_price = $request->price*$request->units;
                    $bid->status = 1;

                    $dueDateTime = Carbon::createFromFormat('d-m-Y', $request->dispatch_date);

                    $bid->dispatch_date = $dueDateTime;


                    if ($bid->saveOrFail()){
                        Session::flash('success', 'Bid has been placed successfully!');
                    }else{
                        Session::flash('error', 'An error occurred when placing the bid.Please try again');
                    }
                }
            });

            return redirect('/profile/purchase_orders/'.$request->purchase_order_id);
        }else{
            Session::flash('error', 'Please complete your profile to continue');
            return redirect('/profile/');
        }

    }

    function bid_details($bid_id)
    {
        $view = 'bid_details';
        $bid = Bid::find($bid_id);

        if (is_null($bid)){
            return view('errors.nothing');
        }else{
            $order = $bid->purchase_order;

            return view('user.index')
                ->withBid($bid)
                ->withOrder($order)
                ->withView($view);
        }

    }

    function approve_bid($bid_id){
        $this->bid = Bid::find($bid_id);

        if (is_null($this->bid)){
            return view('errors.nothing');
        }else{
            $this->bid->status = 2;

            DB::transaction(function () {
                if ($data = $this->bid->update()) {
                    Session::flash("success", "Bid Approved!");

                }
            });

            return redirect('/profile/purchase_orders/'.$bid_id);
        }

    }

    function reject_bid($bid_id){
        $this->bid = Bid::find($bid_id);


        if (is_null($this->bid)){
            return view('errors.nothing');
        }else{
            $this->bid->status = 3;

            DB::transaction(function () {
                if ($data = $this->bid->update()) {
                    Session::flash("error", "Bid Rejected!");

                }
            });

            return redirect('/profile/purchase_orders/'.$bid_id);
        }

    }

    public function create_donor_request(Request $request)
    {

        $this->validate($request, [
            'type' => 'required',
            'product' => 'required',
            'size' => 'required',
            'description' => 'required',
        ]);

        if (Auth::user()->entity_id) {
            DB::transaction(function () {

                $request = (object)$_POST;

                $donorRequest = new DonorRequest();
                $donorRequest->entity_id = Auth::user()->entity_id;
                $donorRequest->type = $request->type;
                $donorRequest->product_id = $request->product;
                $donorRequest->size = $request->size;
                $donorRequest->description = $request->description;

                if ($donorRequest->saveOrFail()){
                    Session::flash('success', 'Donor Request created Successfully!');
                }else{
                    Session::flash('error', 'An error occurred when creating the donor request. Please try again');
                }
            });

            return redirect('/profile/donor_requests');
        }else{
            Session::flash('error', 'Please complete your profile to continue');
            return redirect('/profile/');
        }

    }

    function appeal_details($appeal_id)
    {
        $view = 'appeal_details';
        $donorRequest = DonorRequest::find($appeal_id);

        if (is_null($donorRequest)){
            return view('errors.nothing');
        }else{
            if (Auth::user()->entity_id) {
                $donation = \App\Donation::where('request_id',$donorRequest->id)->where('donor_entity_id',Auth::user()->entity_id)->first();
                return view('user.index')
                    ->withDonorRequest($donorRequest)
                    ->withDonation($donation)
                    ->withView($view);
            }else{
                Session::flash('error', 'Please complete your profile to continue');
                return redirect('/profile/');
            }
        }

    }

    public function create_donation_proposal(Request $request)
    {

        $this->validate($request, [
            'type' => 'required',
            'size' => 'required',
            'terms' => 'required',
        ]);

        if (Auth::user()->entity_id){

            DB::transaction(function () {

                $request = (object)$_POST;

                $donation = new Donation();
                $donation->donor_entity_id = Auth::user()->entity_id;
                $donation->request_id = $request->request_id;
                $donation->type = $request->type;
                $donation->size = $request->size;
                $donation->terms = $request->terms;

                if ($donation->saveOrFail()){
                    Session::flash('success', 'Donation proposal created Successfully!');
                }else{
                    Session::flash('error', 'An error occurred when creating the donation proposal. Please try again');
                }
            });

            return redirect('/profile/donor_appeals/'.$request->request_id);

        }else{
            Session::flash('error', 'Please complete your profile to continue');

            return redirect('/profile/');


        }
    }

    function approve_donation($_id){
        $this->donation = Donation::find($_id);

        if (is_null($this->donation)){
            return view('errors.nothing');
        }else{
            $this->donation->status = 2;

            DB::transaction(function () {
                if ($data = $this->donation->update()) {
                    Session::flash("success", "Donation Accepted!");

                }
            });

            return redirect()->back();
        }

    }

    function reject_donation($_id){
        $this->donation = Donation::find($_id);

        if (is_null($this->donation)){
            return view('errors.nothing');
        }else{
            $this->donation->status = 3;

            DB::transaction(function () {
                if ($data = $this->donation->update()) {
                    Session::flash("error", "Donation Rejected!");

                }
            });

            return redirect()->back();
        }

    }


}
