<?php

namespace App\Http\Controllers;

use App\Bid;
use App\Donation;
use App\Entity;
use App\EntityLobs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class EntityController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create_entity (Request $request)
    {
        $this->validate($request, [
            'terms' => 'required',
            'email_address' => 'required',
            'location' => 'required',
            'industry' => 'required',
            'business_type' => 'required',
            'lob' => 'required',
            'physical_location' => 'required',
            'postal_address' => 'required',
            'phone_no' => 'required',
        ]);

        DB::transaction(function () {

            $request = (object)$_POST;

            $entity = new Entity();
            $entity->industry_id = $request->industry;
            $entity->name = Auth::user()->bus_name;
            $entity->business_type_id = $request->business_type;
            $entity->location = $request->location;
            $entity->physical_location = $request->physical_location;
            $entity->postal_address = $request->postal_address;
            $entity->phone_no = $request->phone_no;
            $entity->email = $request->email_address;
            $entity->terms = $request->terms;


            if ($entity->saveOrFail()){
                foreach ($_POST['lob'] as $lob) {
                    $entityLob = new EntityLobs();
                    $entityLob->entity_id = $entity->id;
                    $entityLob->lob_id = $lob;
                    $entityLob->saveOrFail();
                }

                $user = Auth::user();
                $user->entity_id = $entity->id;
                $user->update();

            }else{
                Session::flash('error', 'An error occurred when creating Business Entity.Please try again');

            }
            Session::flash('success', 'Business Entity Created Successfully!');
        });
        return redirect('/profile');

    }

    function view_supplier($supplier_id)
    {
        $view = 'supplier_details';
        $supplier = Entity::find($supplier_id);


        if (Auth::user()->entity_id) {
            if (is_null($supplier)){
                return view('errors.nothing');
            }else{
                $trade_bids = Bid::select('bids.*','purchase_orders.product_id as product_id')
                    ->join('purchase_orders', 'bids.purchase_order_id', '=', 'purchase_orders.id')
                    ->where('purchase_orders.entity_id','=',Auth::user()->entity_id)
                    ->where('bids.bidder_entity_id','=',$supplier_id)
                    ->orderBy('bids.id', 'desc')
                    ->paginate(15);

                return view('user.index')->withSupplier($supplier)->withBids($trade_bids)->withView($view);
            }
        }else{
            Session::flash('error', 'Please complete your profile to continue');
            return redirect('/profile/');
        }

    }


    function view_donor($donor_id)
    {
        $view = 'donor_details';
        $donor = Entity::find($donor_id);


        if (Auth::user()->entity_id) {
            if (is_null($donor)){
                return view('errors.nothing');
            }else{
                $donations = Donation::join('donor_requests', 'donations.request_id', '=', 'donor_requests.id')
                    ->where('donor_requests.entity_id','=',Auth::user()->entity_id)
                    ->where('donations.donor_entity_id','=',$donor_id)
                    ->orderBy('donations.id', 'desc')
                    ->paginate(15);

                return view('user.index')->withDonor($donor)->withDonations($donations)->withView($view);
            }
        }else{
            Session::flash('error', 'Please complete your profile to continue');
            return redirect('/profile/');
        }

    }
}
