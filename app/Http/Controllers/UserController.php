<?php

namespace App\Http\Controllers;

use App\Bid;
use App\DonorRequest;
use App\Entity;
use App\Product;
use App\PurchaseOrder;
use App\Stock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    function industry_lobs($industry_id)
    {
        echo json_encode(
            DB::table('lobs')->select('lob', 'id')
                ->where('industry_id', '=', $industry_id)
                ->get()
        );
    }



    public function index($view = 'basic')
    {
        $user = Auth::user();
        if (!is_null($user->entity_id)){
            switch (Auth::user()->user_group) {
                case 1:
                    //sme dash
                    $view = 'sme_dashboard';
                    $bids = Bid::select('purchase_orders.product_id','purchase_orders.id as purchase_order_id','purchase_orders.unit_size','purchase_orders.units','bids.bidder_entity_id','bids.total_price')
                                ->join('purchase_orders','bids.purchase_order_id', 'purchase_orders.id')
                                ->where('purchase_orders.entity_id', '=', Auth::user()->entity_id)
                                ->where('bids.status','=',1)
                                ->orderBy('bids.id','desc')
                                ->limit(5)
                                ->get();
                    return view('user.index')->withUser($user)
                        ->withView($view)->withBids($bids);
                    break;
                case 2:
                    //lender/manufacturer dash
                    $view = 'lender_dashboard';
                    return view('user.index')->withUser($user)
                        ->withView($view);
                    break;
                case 3:
                    //donor dash
                    $view = 'donor_dashboard';
                    return view('user.index')->withUser($user)
                        ->withView($view);
                    break;
                case 4:
                    //supplier/distributor dash
                    $view = 'supplier_dashboard';
                    $bids = Bid::where('bidder_entity_id', '=', Auth::user()->entity_id)
                        ->where('status', '=', 2)
                        ->orderBy('id', 'desc')
                        ->limit(5)
                        ->get();
                    $potentialBuyers = PurchaseOrder::select('purchase_orders.*')
                        ->join('bids','purchase_orders.id', 'bids.purchase_order_id')
                        ->where('purchase_orders.entity_id', '!=', Auth::user()->entity_id)
                        ->where('bids.bidder_entity_id', '!=', Auth::user()->entity_id)
                        ->where('purchase_orders.status', '=', 1)
                        ->orderBy('purchase_orders.id', 'desc')
                        ->limit(5)
                        ->get();

                    return view('user.index')
                        ->withPotentialBuyers($potentialBuyers)
                        ->withBids($bids)
                        ->withUser($user)
                        ->withView($view);
                    break;
                default:
                    //unknown
                    $view = 'basic';
                    return view('user.index')->withUser($user)
                        ->withView($view);
            }
        }

        return view('user.index')->withUser($user)
            ->withView($view);
    }

    public function suppliers()
    {
        $view = 'suppliers';

        if (Auth::user()->entity_id) {
            $bid_ids = DB::table('bids')
                ->select('bids.bidder_entity_id as bidder_entity_id')
                ->distinct('bidder_entity_id')
                ->join('purchase_orders', 'bids.purchase_order_id', '=', 'purchase_orders.id')
                ->where('purchase_orders.entity_id','=',Auth::user()->entity_id)
                ->where('bids.status','=',2)
                ->get();

            $ids_array = array();

            foreach($bid_ids as $values){
                for($i=1;$i<=sizeof($bid_ids);$i++){
                    $ids_array[] = $values->bidder_entity_id;
                }
            }

            $suppliers = Entity::whereIn('id', $ids_array)
                ->paginate(15);


            return view('user.index')->withSuppliers($suppliers)->withView($view);
        }else{
            Session::flash('error', 'Please complete your profile to continue');
            return redirect('/profile/');
        }

    }

    public function stock()
    {
        $view = 'stock';

        if (Auth::user()->entity_id) {
            $stocks = Stock::where('entity_id', Auth::user()->entity_id)->orderBy('id', 'desc')->paginate(15);
            return view('user.index')->withStocks($stocks)->withView($view);
        }else{
            Session::flash('error', 'Please complete your profile to continue');
            return redirect('/profile/');
        }

    }

    public function products()
    {
        $view = 'products';

        if (Auth::user()->entity_id) {
            $products = Product::where('entity_id', Auth::user()->entity_id)->orderBy('id', 'desc')->paginate(15);
            return view('user.index')->withProducts($products)->withView($view);
        }else{
            Session::flash('error', 'Please complete your profile to continue');
            return redirect('/profile/');
        }

    }


    function product_details($product_id)
    {
        $view = 'product_details';
        $product = Product::find($product_id);

        if (is_null($product) || $product->entity_id != Auth::user()->entity_id){
            return view('errors.nothing');
        }else{
            return view('user.index')
                ->withProduct($product)
                ->withView($view);
        }

    }

    public function purchase_orders()
    {
        $view = 'purchase_orders';

        if (Auth::user()->entity_id) {
            $orders= PurchaseOrder::where('entity_id', Auth::user()->entity_id)->orderBy('id', 'desc')->paginate(15);
            return view('user.index')->withOrders($orders)->withView($view);
        }else{
            Session::flash('error', 'Please complete your profile to continue');
            return redirect('/profile/');
        }

    }

    public function bids()
    {
        $view = 'bids';

        if (Auth::user()->entity_id) {
            $bids = Bid::where('bidder_entity_id', Auth::user()->entity_id)->orderBy('id', 'desc')->paginate(15);
            return view('user.index')->withBids($bids)->withView($view);
        }else{
            Session::flash('error', 'Please complete your profile to continue');
            return redirect('/profile/');
        }

    }

    public function donors()
    {
        $view = 'donors';

        if (Auth::user()->entity_id) {
            $donors = Entity::select('entities.*')
                ->join('donations', 'entities.id', '=', 'donations.donor_entity_id')
                //->join('donor_requests', 'entities.id', '=', 'donations.donor_entity_id')
                ->distinct('donations.donor_entity_id')
                ->orderBy('entities.id', 'desc')
                ->paginate(15);


            return view('user.index')->withDonors($donors)->withView($view);
        }else{
            Session::flash('error', 'Please complete your profile to continue');
            return redirect('/profile/');
        }
    }


     public function donor_appeals()
    {
        $view = 'donor_appeals';
        $requests = DonorRequest::where('status',1)->orderBy('id', 'desc')->paginate(15);

        return view('user.index')->withRequests($requests)->withView($view);

    }

    public function donor_requests()
    {
        $view = 'donor_requests';

        if (Auth::user()->entity_id) {
            $requests = DonorRequest::where('entity_id', Auth::user()->entity_id)->orderBy('id', 'desc')->paginate(15);
            return view('user.index')->withRequests($requests)->withView($view);

        }else{
            Session::flash('error', 'Please complete your profile to continue');
            return redirect('/profile/');
        }
    }

    public function settings()
    {
        $view = 'settings';

        return view('user.index')->withView($view);

    }

}
