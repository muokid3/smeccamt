<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Mail;

class MailController extends Controller
{

    public function contact_us_email()
    {
        $data = Input::all();
        //Validation rules
        $rules = array(
            'fname' => 'required',
            'email' => 'required|email',
            'message' => 'required|min:5'
        );

        //Validate data
        $validator = Validator::make($data, $rules);

        //If everything is correct than run passes.
        if ($validator->passes()) {

            Mail::send('mailer.contact_us', ['data' => $data], function ($message) use ($data) {
                //$message->from($data['email'] , $data['first_name']); uncomment if using first name and email fields
                $message->from($data['email'], $data['fname'] . ' ' . $data['lname']);
                //email 'To' field: cahnge this to emails that you want to be notified.
                $message->to('info@metropol.co.ke', 'SMECCA Admin')->cc($data['email'])->subject($data['subject']);

            });
            // Redirect to page
            Session::flash('success', 'Your message has been sent successfully. Our team will get back to you');
            return redirect('contact');
            //return View::make('contact');
        } else {
            //return contact form with errors
            return redirect('contact')
                ->with('error', 'Feedback must contain more than 5 characters. Try Again.');
        }
    }

}
