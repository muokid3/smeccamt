<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrder extends Model
{
    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }

    public function entity()
    {
        return $this->belongsTo('App\Entity', 'entity_id');
    }

    public function bids()
    {
        return count(\App\Bid::where('purchase_order_id', '=', $this->id)->get());
    }
}
