<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPermissions extends Model
{
    public function get_permission()
    {
        return $this->belongsTo('App\Permission', 'permission');
    }
}
