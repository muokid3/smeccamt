<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
    public function donation_status()
    {

        switch ($this->status) {
            case 1:
                return "Pending";
                break;
            case 2:
                return "Accepted";
                break;
            default:
                return "Not Accepted";

        }
    }

    public function donation_request()
    {
        return $this->belongsTo('App\DonorRequest', 'request_id');
    }

    public function donor()
    {
        return $this->belongsTo('App\Entity', 'donor_entity_id');
    }
}
