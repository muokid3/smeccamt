<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonorRequest extends Model
{
    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }

    public function entity()
    {
        return $this->belongsTo('App\Entity', 'entity_id');
    }
}
