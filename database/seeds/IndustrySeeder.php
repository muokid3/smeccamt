<?php

use Illuminate\Database\Seeder;

class IndustrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('industries')->truncate();
        $json = File::get("database/data/industry.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
            \App\Industry::firstOrCreate(array(
                'id' => $obj->id,
                'industry' => $obj->industry
            ));
        }
    }
}
