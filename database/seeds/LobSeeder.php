<?php

use Illuminate\Database\Seeder;

class LobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lobs')->truncate();
        $json = File::get("database/data/lob.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
            \App\Lob::firstOrCreate(array(
                'id' => $obj->id,
                'industry_id' => $obj->industry_id,
                'lob' => $obj->lob
            ));
        }
    }
}
