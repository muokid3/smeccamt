<?php

use Illuminate\Database\Seeder;

class PaymentModeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { DB::table('payment_modes')->truncate();
        $json = File::get("database/data/payment_modes.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
            \App\PaymentMode::firstOrCreate(array(
                'id' => $obj->id,
                'mode' => $obj->lob
            ));
        }//
    }
}
