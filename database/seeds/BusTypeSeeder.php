<?php

use Illuminate\Database\Seeder;

class BusTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('business_types')->truncate();
        $json = File::get("database/data/bustype.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
            \App\BusinessType::firstOrCreate(array(
                'id' => $obj->id,
                'business_type' => $obj->business_type
            ));
        }
    }
}
