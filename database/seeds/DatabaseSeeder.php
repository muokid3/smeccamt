<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(IndustrySeeder::class);
        $this->call(LobSeeder::class);
        $this->call(BusTypeSeeder::class);
        $this->call(PaymentModeSeeder::class);
    }
}
