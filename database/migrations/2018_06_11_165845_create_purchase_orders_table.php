<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lob_id');
            $table->integer('product_id');
            $table->integer('entity_id');
            $table->string('location');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('unit_size');
            $table->integer('units');
            $table->boolean('delivery');
            $table->boolean('status')->default(1);
            $table->text('terms');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_orders');
    }
}
