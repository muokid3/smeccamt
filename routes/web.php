<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/get_user_group/{user_group}', 'HomeController@get_user_group');
Route::get('/contact', 'HomeController@contact');
Route::post('/sendemail', 'MailController@contact_us_email');



Route::group(['middleware' => ['auth']], function () {

    Route::get('/industry_lobs/{industry}', 'UserController@industry_lobs');



    Route::get('/logout', 'Auth\LoginController@logout');
    Route::get('/profile', 'UserController@index');
    Route::post('/create_entity', 'EntityController@create_entity');

    Route::get('/profile/suppliers', 'UserController@suppliers');
    Route::get('/profile/suppliers/{supplier_id}', 'EntityController@view_supplier');



    Route::get('/profile/stock', 'UserController@stock');
    Route::post('/profile/stock/add_stock', 'ProductController@add_stock');

    Route::get('/profile/products', 'UserController@products');
    Route::get('/profile/products/{product_id}', 'UserController@product_details');
    Route::post('/profile/products/create', 'ProductController@create');
    Route::post('/profile/products/edit', 'ProductController@edit');

    Route::get('/profile/purchase_orders', 'UserController@purchase_orders');
    Route::post('/profile/purchase_orders/create_purchase_order', 'ProductController@create_purchase_order');
    Route::get('/profile/purchase_orders/{order_id}', 'ProductController@order_details');
    Route::post('/profile/purchase_orders/place_bid', 'ProductController@place_bid');




    Route::get('/profile/find_orders', 'ProductController@find_orders');
    Route::post('/profile/purchase_orders/create_purchase_order', 'ProductController@create_purchase_order');


    Route::get('/profile/bids', 'UserController@bids');
    Route::get('/profile/bid/{bid_id}', 'ProductController@bid_details');
    Route::get('/profile/bid/approve/{bid_id}', 'ProductController@approve_bid');
    Route::get('/profile/bid/reject/{bid_id}', 'ProductController@reject_bid');


    //sme
    Route::get('/profile/donors', 'UserController@donors');
    Route::get('/profile/donors/{donor_id}', 'EntityController@view_donor');
    Route::get('/profile/donor_requests', 'UserController@donor_requests');
    Route::post('/profile/donor_requests/new', 'ProductController@create_donor_request');
    Route::get('/profile/donor_requests/{appeal_id}', 'ProductController@appeal_details');
    Route::get('/profile/donor_requests/approve/{_id}', 'ProductController@approve_donation');
    Route::get('/profile/donor_requests/reject/{_id}', 'ProductController@reject_donation');


    //lenders and donors
    Route::get('/profile/donor_appeals', 'UserController@donor_appeals');
    Route::get('/profile/donor_appeals/{appeal_id}', 'ProductController@appeal_details');
    Route::post('/profile/donor_requests/new_donation_proposal', 'ProductController@create_donation_proposal');


//    Route::get('/profile/donor_requests', 'UserController@donor_requests');
//    Route::post('/profile/donor_requests/new', 'ProductController@create_donor_request');


    Route::get('/profile/settings', 'UserController@settings');



});


