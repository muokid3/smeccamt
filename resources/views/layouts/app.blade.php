<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">     <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>SMECCA | {{isset($page_title)?$page_title : ''}}</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <!-- Mobile Specific Metas
      ================================================== -->
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <!-- CSS
      ================================================== -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="{{ url('css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('css/custom.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('css/font-awesome.css') }}" rel="stylesheet" type="text/css">
    <!-- SCRIPTS
      ================================================== -->
    <script>window.Laravel =<?php echo json_encode(['csrfToken' => csrf_token(),]); ?>     </script>
    @yield('css')

</head>
<body class="home">
<!--[if lt IE 7]>
<p class="chromeframe">You are using an outdated browser. <a href="https://browsehappy.com/">Upgrade your browser
    today</a> or <a href="https://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better
    experience this site.</p>
<![endif]-->
<div class="body">
    @include('common.header')
    @yield('content')
    @include('common.footer')
</div>
<!-- Scripts -->

<link href="{{url('/form-select2/select2.css')}}" type="text/css" rel="stylesheet"/>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-3.0.0.js"></script>
<script src="https://code.jquery.com/jquery-migrate-3.0.0.js"></script>
<script src="{{ url('js/modernizr.js') }}"></script><!-- Modernizr -->
<script type="text/javascript" src="{{ url('/js/moment.js')}}"></script>
<script src="{{ url('/js/bootstrap.js') }}"></script> <!-- UI -->
<script src="{{ url('/js/datetimepicker.js') }}"></script> <!-- All Scripts -->
@yield('script')
<script src="{{ url('/js/bootbox.min.js')}}"></script>                                  <!-- Load Bootstrap -->
<script src="{{ url('/form-select2/select2.min.js') }}"></script>
<!-- Advanced Select Boxes -->
@yield('scripts')
<script>
    $(function () {
        $("#deliveryDate").datetimepicker({
            //viewMode: 'years',
            format: 'DD-MM-YYYY'
            //maxDate: moment().subtract(18, 'years')
        });
    });
</script>


<script>
    $(document).ready(function () {
        $("select").removeClass('form-control');
        $("select").select2({width: '100%'});
    });
</script>

<!-- Modal -->
<div class="modal fade" id="bidModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Place a Bid </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form-horizontal" role="form" method="POST" action="{{ url('profile/purchase_orders/place_bid') }}">
                    {{ csrf_field() }}
                <div class="modal-body">


                        @if(isset($order))
                            <div class="row">
                                <div class="col-md-12">
                                    <h4><span style="color: red; ">Order Details</span></h4>
                                    <hr/>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <label>Product: </label>
                                    {{$order->product->product_name}} - ({{$order->unit_size}})

                                </div>

                                <div class="col-md-6">
                                    <label>Required Units:</label>
                                    {{$order->units}}

                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-6">
                                    <label for="unit_size">Delivery: </label>
                                    {{$order->delivery ? 'Delivery is required' : 'Delivery is not required'}}

                                </div>

                                <div class="col-md-6">
                                    <label for="units">Date Created: </label>
                                    {{\Carbon\Carbon::parse(date("d-m-Y", strtotime($order->created_at)))
                                                                          ->formatLocalized('%A %d %B %Y')}}
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <label>Location</label>
                                    {{$order->location}}
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <h4><span style="color: red; ">Bid Details</span></h4>
                                    <hr/>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-md-6">
                                    <label>Units to be Supplied</label>
                                    <input type="hidden" name="purchase_order_id" value="{{$order->id}}">
                                    <input type="number" name="units" required autofocus value="{{old('units')}}"
                                           class="form-control">
                                    <div style="color: red">{{$errors->first("units") }}</div>
                                </div>

                                <div class="col-md-6">
                                    <label>Unit Price (Ksh)</label>
                                    <input type="number" name="price" required  value="{{old('price')}}"
                                           class="form-control">
                                    <div style="color: red">{{$errors->first("price") }}</div>
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-6">
                                    <label>Dispatch/Collection Date</label>
                                    <input type="text" name="dispatch_date" required id="deliveryDate" value="{{old('dispatch_date')}}"
                                           class="form-control datepicker">
                                    <div style="color: red">{{$errors->first("dispatch_date") }}</div>
                                </div>

                                <div class="col-md-6">
                                    <label>Payment Mode</label>
                                    {!! Form::select('mode', \App\PaymentMode::pluck('mode', 'id'), "",
                                                    ['class' => 'select2 form-control', 'id'=>'mode']) !!}
                                    <div style="color: red">{{$errors->first("mode") }}</div>
                                </div>

                            </div>
                        @endif

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Place</button>
                </div>

            </form>


        </div>
    </div>
</div>


</body>
</html>