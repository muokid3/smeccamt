@extends('layouts.app')

@section('content')
    <section class="section swatch-white-red has-top p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="box-round box-big">
                        <div class="box-dummy"></div>
                        <span class="box-inner">
					<i class="fa fa-frown-o" data-animation="tada"></i>
				</span>
                    </div>
                </div>
            </div>
            <header class="section-header underline text-center">
                <h1 class="headline super hairline">Whoops!</h1>
                <p class="">Sorry, Looks like something went wrong. Head back to the home page and start from there</p>
            </header>
            <div class="text-center">
                <a class="btn btn-primary btn-lg btn-icon-right pull-center" href="{{url('/')}}">
                    take me home
                    <div class="hex-alt hex-alt-big">
                        <i class="fa fa-home" data-animation="tada"></i>
                    </div>
                </a>
            </div>
        </div>
    </section>
@endsection
