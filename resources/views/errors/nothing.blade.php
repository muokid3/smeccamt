
<!DOCTYPE html>
<html lang="en">
<head>
    <title>404 Nothing here</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <style type="text/css">
        html, body, div, span, applet, object, iframe,
        h1, h2, h3, h4, h5, h6, p, blockquote, pre,
        a, abbr, acronym, address, big, cite, code,
        del, dfn, em, img, ins, kbd, q, s, samp,
        small, strike, strong, sub, sup, tt, var,
        b, u, i, center,
        dl, dt, dd, ol, ul, li,
        fieldset, form, label, legend,
        table, caption, tbody, tfoot, thead, tr, th, td,
        article, aside, canvas, details, embed,
        figure, figcaption, footer, header, hgroup,
        menu, nav, output, ruby, section, summary,
        time, mark, audio, video {
            margin: 0;
            padding: 0;
            border: 0;
            font-size: 100%;
            font: inherit;
            vertical-align: baseline;
        }
        /* HTML5 display-role reset for older browsers */
        article, aside, details, figcaption, figure,
        footer, header, hgroup, menu, nav, section {
            display: block;
        }
        body {
            line-height: 1;
        }
        ol, ul {
            list-style: none;
        }
        blockquote, q {
            quotes: none;
        }
        blockquote:before, blockquote:after,
        q:before, q:after {
            content: '';
        }
        table {
            border-collapse: collapse;
            border-spacing: 0;
        }

        html {
            height: 100%;
        }

        body {
            font-family: -apple-system, ".SFNSText-Regular", "Helvetica Neue", "Roboto", "Segoe UI", sans-serif;
            -webkit-font-smoothing: antialiased;
            font-size: 16px;
            min-height: 100%;
            background-color: #000;
            color: #fff;
        }

        a {
            text-decoration: none;
        }

        .background {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: url('{{url('/images/bg.gif')}}') center center no-repeat;
            background-size: cover;
            z-index: -1;
            opacity: 0.2;

        }

        header {
            height: 48px;
            background-color: #000;
        }

        .logo {
            display: block;
            width: 64px;
            height: 48px;
        }

        .message {
            text-align: center;
            position: absolute;
            width: 100%;
            top: 50%;
            transform: translateY(-50%);
        }

        .message h1 {
            font-size: 144px;
            font-weight: 100;
            margin-bottom: 16px;
        }

        .message h2 {
            margin-bottom: 24px;
        }

        .message h3 {
            font-weight: bold;
            font-size: 16px;
            margin-bottom: 32px;
        }

        .lost {
            display: block;
            position: absolute;
            bottom: 0;
            left: 50%;
            margin-left: -111px;
        }

        .btn-download {
            display: inline-block;
            line-height: 20px;
            padding: 12px 24px 12px 48px;
            background: #09f url(../img/404/download.svg) left -1px no-repeat;
            color: #fff;
            font-weight: bold;
            border-radius: 2px;
        }

        .btn-download:hover {
            background-color: #3caffc;
        }.toast {
             position: absolute;
             width: 100%;
             top: 100px;
             left: 0;
             z-index: 13;
             color: #000;
             text-align: center;
             height: 0;
         }

        .toast p {
            display: inline-block;
            background-color: #ffe168;
            padding: 5px 10px;
            line-height: 20px;
            border-radius: 3px;
            box-shadow: 0 3px 5px rgba(0, 0, 0, .3);
            position: relative;
        }

        .toast p.close {
            padding-right: 35px;
        }

        .toast.toast-color-green p {
            background-color: #690;
        }

        .toast.toast-color-yellow p {
            background-color: #ffe168;
        }

        .toast.toast-color-red p {
            background-color: #c00;
        }

        .toast a.btn-close {
            color: #000;
            position: absolute;
            top: 5px;
            right: 10px;
        }
    </style>
</head>
<body>

    <div class="background">
    </div><!-- / background -->

    <div class="message">
        <h1>404</h1>
        <h2>There's nothing here.</h2>
        <h3>It seems you got lost along the way...</h3>
        <a class="btn-download" href="{{url('/profile')}}">Take me Home</a>
    </div><!-- / message -->

</body>

</html>

