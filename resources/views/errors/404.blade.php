<?php $pate_title ="Page not found"?>
@extends('layouts.app')

@section('content')
    <section class="section swatch-red-white">
        <div class="container">
            <header class="section-header no-underline">
                <h1 class="headline hyper hairline text-center">Page not found!</h1>
            </header>
            <div class="text-center">
                <img src="/images/404.jpg" alt="Page not found!">
            </div>
        </div>
    </section>
    <section class="section swatch-white-red has-top">

        <div class="container">
            <header class="section-header underline text-center">
                <h1 class="headline super hairline">What now?</h1>
                <p class="">Sorry, the page you are looking for cannot be found here. Head back to the home page and start from there</p>
            </header>
            <div class="text-center">
                <a class="btn btn-primary btn-lg btn-icon-right pull-center" href="{{url('/')}}">
                    take me home
                    <div class="hex-alt hex-alt-big">
                        <i class="fa fa-home" data-animation="tada"></i>
                    </div>
                </a>
            </div>
        </div>
    </section>
@endsection
