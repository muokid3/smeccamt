<?php $page_title = "Profile ";

$user = Auth::user();
?>
@extends('layouts.app')

@section('content')
    <!-- Start Body Content -->
    <div class="main main-unraised">
        <div class="section">
            <div class="row">
                <div class="col-md-2 col-md-offset-1 col-sm-12 col-xs-12">
                    <ul class="members-list panel row list-unstyled">
                        <li style="margin-bottom: 10px" class="text-center">
                            <a style="align-content: center !important;" href="{{url('/profile')}}">
                                <img src="{{url('/images/speaker1.jpg')}}" alt=""
                                     width="60" height="60">
                            </a>
                        </li>
                        <li class="text-left">
                            <strong>NAME:</strong> <a
                                    href="{{url('/profile')}}">{{$user->name}}</a><br/>
                            <strong>EMAIL: </strong>{{$user->email}}<br/>
                            <strong>ENTITY: </strong>{{$user->bus_name}}<br/>
                            <strong>TYPE: </strong>{{$user->type->name}}<br/>
                            <div class="spacer-20"></div>
                            <div class="meta-data">

                                    @if (is_null($user->entity_id))

                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="60"
                                             aria-valuemin="0" aria-valuemax="100" style="width:60%">
                                            Profile 60% Complete
                                        </div>
                                    </div>
                                    @else

                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="100"
                                             aria-valuemin="0" aria-valuemax="100" style="width:100%">
                                            Profile 100% Complete
                                        </div>
                                    </div>
                                    @endif


                            </div>
                        </li>


                        <div class="spacer-20"></div>


                        <li>
                            <a href="{{url('/profile')}}"
                               class="list-group-item {{checkActiveRoute('profile')}}"><i
                                        class="fa fa-user"></i> Profile </a>
                        </li>

                        @if($user->user_group == 1)
                            {{--sme--}}

                            <li>
                                <a href="{{url('/profile/products')}}"
                                   class="list-group-item {{checkActiveRoute('profile/products')}}">
                                    <i class="fa fa-database"></i> My Products</a>
                            </li>

                            <li>
                                <a href="{{url('/profile/stock')}}"
                                   class="list-group-item {{checkActiveRoute('profile/stock')}}">
                                    <i class="fa fa-list-alt"></i> Stock</a>
                            </li>


                            <li>
                                <a href="{{url('/profile/purchase_orders')}}"
                                   class="list-group-item {{checkActiveRoute('profile/purchase_orders')}}">
                                    <i class="fa fa-shopping-cart"></i> My Purchase Orders</a>
                            </li>


                            <li>
                                <a href="{{url('/profile/suppliers')}}"
                                   class="list-group-item {{checkActiveRoute('profile/suppliers')}}"><i
                                            class="fa fa-truck"></i> My Suppliers</a>


                            </li>


                            <li>
                                <a href="{{url('/profile/donor_requests')}}"
                                   class="list-group-item {{checkActiveRoute('profile/donor_requests')}}">
                                    <i class="fa fa-circle-o-notch"></i> Donor Requests</a>

                            </li>


                            <li>
                                <a href="{{url('/profile/donors')}}"
                                   class="list-group-item {{checkActiveRoute('profile/donors')}}"><i
                                            class="fa fa-dribbble"></i> My Donors</a>

                            </li>


                            <li>
                                {{--<a href="{{url('/profile/settings')}}"--}}
                                   {{--class="list-group-item {{checkActiveRoute('profile/settings')}}"><i--}}
                                            {{--class="fa fa-cog"></i> Account Settings</a>--}}

                                <a href="#" onclick="comingSoon()"
                                   class="list-group-item {{checkActiveRoute('profile/settings')}}"><i
                                            class="fa fa-cog"></i> Account Settings</a>
                            </li>


                        @elseif($user->user_group == 2)
                            {{--Lender/Manufacturer--}}

                            <li>
                                <a href="{{url('/profile/products')}}"
                                   class="list-group-item {{checkActiveRoute('profile/products')}}">
                                    <i class="fa fa-database"></i> My Products</a>
                            </li>

                            <li>
                                <a href="{{url('/profile/stock')}}"
                                   class="list-group-item {{checkActiveRoute('profile/stock')}}">
                                    <i class="fa fa-list-alt"></i> Stock</a>
                            </li>


                            <li>
                                <a href="{{url('/profile/find_orders')}}"
                                   class="list-group-item {{checkActiveRoute('profile/find_orders')}}">
                                    <i class="fa fa-search-plus"></i> Find Buyer</a>
                            </li>

                            <li>
                                <a href="{{url('/profile/donor_appeals')}}"
                                   class="list-group-item {{checkActiveRoute('profile/donor_appeals')}}">
                                    <i class="fa fa-heart-o"></i> Find Donation Appeals</a>
                            </li>

                            <li>
                                <a href="#" onclick="comingSoon()"
                                   class="list-group-item {{checkActiveRoute('profile/settings')}}"><i
                                            class="fa fa-cog"></i> Account Settings</a>
                            </li>


                        @elseif($user->user_group == 3)
                            {{--Donor--}}


                            <li>
                                <a href="{{url('/profile/products')}}"
                                   class="list-group-item {{checkActiveRoute('profile/products')}}">
                                    <i class="fa fa-database"></i> My Products</a>
                            </li>

                            {{--<li>--}}
                                {{--<a href="{{url('/profile/stock')}}"--}}
                                   {{--class="list-group-item {{checkActiveRoute('profile/stock')}}">--}}
                                    {{--<i class="fa fa-list-alt"></i> Stock</a>--}}
                            {{--</li>--}}


                            {{--<li>--}}
                                {{--<a href="{{url('/profile/find_orders')}}"--}}
                                   {{--class="list-group-item {{checkActiveRoute('profile/find_orders')}}">--}}
                                    {{--<i class="fa fa-search-plus"></i> Find Buyer</a>--}}
                            {{--</li>--}}

                            <li>
                                <a href="{{url('/profile/donor_appeals')}}"
                                   class="list-group-item {{checkActiveRoute('profile/donor_appeals')}}">
                                    <i class="fa fa-heart-o"></i> Find Donation Appeals</a>
                            </li>

                            <li>
                                <a href="#" onclick="comingSoon()"
                                   class="list-group-item {{checkActiveRoute('profile/settings')}}"><i
                                            class="fa fa-cog"></i> Account Settings</a>
                            </li>

                        @elseif($user->user_group == 4)
                            {{--Supplier (Distributor)--}}

                            <li>
                                <a href="{{url('/profile/products')}}"
                                   class="list-group-item {{checkActiveRoute('profile/products')}}">
                                    <i class="fa fa-database"></i> My Products</a>
                            </li>

                            <li>
                                <a href="{{url('/profile/stock')}}"
                                   class="list-group-item {{checkActiveRoute('profile/stock')}}">
                                    <i class="fa fa-list-alt"></i> Stock</a>
                            </li>

                            <li>
                                <a href="{{url('/profile/purchase_orders')}}"
                                   class="list-group-item {{checkActiveRoute('profile/purchase_orders')}}">
                                    <i class="fa fa-shopping-cart"></i> My Purchase Orders</a>
                            </li>

                            <li>
                                <a href="{{url('/profile/find_orders')}}"
                                   class="list-group-item {{checkActiveRoute('profile/find_orders')}}">
                                    <i class="fa fa-search-plus"></i> Find Buyer</a>
                            </li>

                            <li>
                                <a href="{{url('/profile/bids')}}"
                                   class="list-group-item {{checkActiveRoute('profile/bids')}}">
                                    <i class="fa fa-clipboard"></i> My Bids</a>
                            </li>

                            <li>
                                <a href="#" onclick="comingSoon()"
                                   class="list-group-item {{checkActiveRoute('profile/settings')}}"><i
                                            class="fa fa-cog"></i> Account Settings</a>
                            </li>

                        @endif
                    </ul>
                </div>

                <div class="col-md-9 col-sm-12 col-xs-12 " style="padding: 0 20px !important;">
                    <div class="row">
                        <div class="col-sm-12">
                            @if(Session::has('message'))
                                <div class="alert alert-info">{{Session::get('message')}}</div>
                            @elseif(Session::has('success'))
                                <div class="alert alert-success">{{Session::get('success')}}</div>
                            @elseif(Session::has('error'))
                                <div class="alert alert-danger">{{Session::get('error')}}</div>
                            @endif

                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                        @include('user.'.$view)
                        <!-- /.tab-content -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('script')
    <script src="{{ url('sweet/sweetalert2/sweetalert2.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ url('sweet/sweetalert2/sweetalert2.css') }}">
    <script type="text/javascript">
        function comingSoon () {
            swal(
                'Coming Soon',
                'Work in progress, will be right back!',
                'info'
            )
        }

    </script>
@endsection