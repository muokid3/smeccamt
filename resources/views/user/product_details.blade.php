


    <div class="container-fluid">
        <div class="row">

                <h3 class="text-center">
                    {{Auth::user()->bus_name}}
                </h3>
                <h5 class="text-center">Edit your product</h5>

            <hr/>

            <a href="{{url('profile/products')}}" id="toTop" class="btn btn-info"><span class="fa fa-backward"></span> Back to Products</a>

            <div style="margin-top: 3%" class="col-md-12">
                        <!-- Nav tabs --><div class="card">

                    <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#edit-product" aria-controls="edit-product" role="tab" data-toggle="tab">Edit Product</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="edit-product">
                                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/profile/products/edit') }}">
                                        {{ csrf_field() }}
                                        <div class="col-md-6">
                                            <input type="hidden" name="product_id" value="{{$product->id}}">
                                            <label for="product_name">Product Name</label>
                                            {!! Form::text('product_name', $product->product_name,['class'=>'form-control','id'=>'product_name']) !!}

                                        </div>

                                        <div class="col-md-6">
                                            <label for="product_code">Product Code</label>
                                            {!! Form::text('product_code', $product->product_code,['class'=>'form-control','id'=>'product_code']) !!}

                                        </div>

                                        <div class="col-md-12" style="margin-top: 2%">
                                            <input type="submit" value="Update" class="btn btn-primary">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
        </div>

    </div>

