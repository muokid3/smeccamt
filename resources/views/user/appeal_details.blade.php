


    <div class="container-fluid">
        <div class="row">

                <h3 class="text-center">
                    {{Auth::user()->bus_name}}
                </h3>
                <h5 class="text-center">Purchase Order Details </h5>
            <hr/>

            <h2 style="text-align: center; margin-bottom: 2%">{{$donorRequest->product->product_name}} - Donation Appeal</h2>

            <div class="col-md-12">

                <!-- Nav tabs -->
                <div class="card">

                    <!-- Tab panes -->
                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane active" id="add-purchase-order">
                                <div class="row">
                                    <input type="hidden" id="lat" name="lat">
                                    <input type="hidden" id="lng" name="lng">
                                    <div class="column">
                                        <div class="col-sm-6 col-md-6 col-xs-12">

                                            <div class="col-md-12">
                                                <h4><span style="color: red; ">Request Details</span> </h4>
                                                <hr/>
                                            </div>


                                            <div class="col-md-6">
                                                <label>Request Type: </label>
                                                {{$donorRequest->type}}

                                            </div>

                                            <div class="col-md-6">
                                                <label>Product: </label>
                                                {{$donorRequest->product->product_name}}

                                            </div>

                                            <div class="col-md-6">
                                                <label>Quantity/amount Required:</label>
                                                {{$donorRequest->size}}

                                            </div>


                                            <div class="col-md-6">
                                                <label for="units">Date Created: </label>
                                                {{\Carbon\Carbon::parse(date("d-m-Y", strtotime($donorRequest->created_at)))
                                                                                      ->formatLocalized('%A %d %B %Y')}}
                                            </div>
                                            <div class="col-md-12">
                                                <label>Description</label>
                                                {{$donorRequest->description}}

                                            </div>

                                            @if(Auth::user()->entity_id != $donorRequest->entity_id)
                                                <div style="margin-top: 2%" class="col-md-12">
                                                    <h4><span style="color: red; ">Client Details</span> </h4>
                                                    <hr/>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Client Name: </label>
                                                    {{$donorRequest->entity->name}}
                                                </div>

                                                <div class="col-md-12">
                                                    <label>Business Phone: </label>
                                                    {{$donorRequest->entity->phone_no}}
                                                </div>
                                                <div class="col-md-12">
                                                    <label>E-Mail: </label>
                                                    {{$donorRequest->entity->email}}
                                                </div>

                                                <div class="col-md-12">
                                                    <label>Physical Location: </label>
                                                    {{$donorRequest->entity->physical_location}}
                                                </div>
                                            @endif
                                        </div>

                                    </div>

                                    @if($donorRequest->entity_id != Auth::user()->entity_id)
                                        <div class="column">
                                            <div class="col-sm-6 col-md-6 hidden-xs" style="height: 500px;width: 50%; ">

                                                <div style="margin-top: 2%" class="col-md-12">
                                                    <h4><span style="color: red; ">Donation Proposal</span> </h4>
                                                    <hr/>
                                                </div>

                                                @if(!is_null($donation))
                                                    <p><i style="color: red; ">You have made a donation proposal to this request with the following details</i> </p>

                                                    <div class="col-md-12">
                                                        <label>Donation Type: </label>
                                                        {{$donation->type}}

                                                    </div>

                                                    <div class="col-md-12">
                                                        <label>Size/Amount: </label>
                                                        {{$donation->size}}

                                                    </div>

                                                    <div class="col-md-12">
                                                        <label>Status:</label>
                                                        {{$donation->donation_status()}}

                                                    </div>


                                                    <div class="col-md-12">
                                                        <label for="units">Date Created: </label>
                                                        {{\Carbon\Carbon::parse(date("d-m-Y", strtotime($donation->created_at)))
                                                                                              ->formatLocalized('%A %d %B %Y')}}
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label>Terms</label>
                                                        <br>
                                                        {{$donation->terms}}

                                                    </div>
                                                @else
                                                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/profile/donor_requests/new_donation_proposal') }}">
                                                        {{ csrf_field() }}

                                                        <div class="row">

                                                            <input type="hidden" name="request_id" value="{{$donorRequest->id}}">

                                                            <div class="col-md-12">
                                                                <label>Donation Type</label>
                                                                <select name="type" class="select2 form-control">
                                                                    <option value="cash">Cash</option>
                                                                    <option value="Goods/Services">Goods/Services</option>
                                                                </select>
                                                            </div>


                                                            <div class="col-md-12">
                                                                <label>Size/Amount</label>
                                                                <input type="text" name="size" required value="{{old('size')}}" class="form-control">
                                                                <div style="color: red">{{$errors->first("size") }}</div>

                                                            </div>


                                                            <div class="col-sm-12">
                                                                <label>Terms (Please provide your terms for the donation)</label>
                                                                <textarea name="terms" class="form-control" required placeholder="" rows="6">{{old('terms')}}</textarea>
                                                                <div style="color: red">{{$errors->first("terms") }}</div>
                                                            </div>

                                                            <div class="col-md-12" style="margin-top: 2%">
                                                                <input type="submit" value="Create" class="btn btn-block btn-primary align-left">
                                                            </div>

                                                        </div>

                                                    </form>
                                                @endif

                                            </div>
                                        </div>
                                    @endif
                                </div>

                            @if(Auth::user()->entity_id == $donorRequest->entity_id)
                                <div class="row">

                                    <div style="margin-top: 2%" class="col-md-12">
                                        <h4><span style="color: red; ">Donor Offers ({{\App\Donation::where('request_id',$donorRequest->id)->count()}})</span> </h4>
                                        <hr/>
                                    </div>

                                    <table class="table table-striped table-responsive">
                                        <thead>
                                        <tr>
                                            <th>Donor</th>
                                            <th>Donation Type</th>
                                            <th>Size/Amount</th>
                                            <th class="hidden-xs">Date Created</th>
                                            <th class="hidden-xs">Terms</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach(\App\Donation::where('request_id','=',$donorRequest->id)->orderBy('id', 'desc')->get() as $donationOffer)

                                            <tr>

                                                <td>
                                                    {{$donationOffer->donor->name}}
                                                </td>
                                                <td>
                                                    {{$donationOffer->type}}
                                                </td>

                                                <td>
                                                    {{$donationOffer->size}}
                                                </td>

                                                <td class="hidden-xs">
                                                    {{\Carbon\Carbon::parse(date("d-m-Y", strtotime($donationOffer->created_at)))
                                                                                              ->formatLocalized('%A %d %B %Y')}}
                                                </td>

                                                <td class="hidden-xs">
                                                    {{$donationOffer->terms}}
                                                </td>


                                                <td>
                                                    @if($donationOffer->status == 1)
                                                        <a style="margin-right: 5px" href="javascript:void(0);" onclick="acceptOffer('{{$donationOffer->donor->name}}','{{$donationOffer->id}}');">
                                                                        <span class="label label-primary">
                                                                            Accept
                                                                        </span>
                                                        </a>

                                                        <a href="javascript:void(0);" onclick="rejectOffer('{{$donationOffer->donor->name}}','{{$donationOffer->id}}');">
                                                                        <span class="label label-danger">
                                                                            Reject
                                                                        </span>
                                                        </a>
                                                    @elseif($donationOffer->status == 2)
                                                        <span class="label label-success">{{$donationOffer->donation_status()}}</span>
                                                    @else
                                                        <span class="label label-warning">{{$donationOffer->donation_status()}}</span>
                                                    @endif


                                                </td>

                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>

                                </div>
                            @endif


                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>


    @section('scripts')
        <script>

            function acceptOffer(nm,ID){
                bootbox.confirm("Are you sure you want to ACCEPT offer from \"" + nm + "\" ? ", function(result) {
                    if(result) {

                        $.ajax({
                            url: '/profile/donor_requests/approve/' + ID,
                            type: 'get',
                            headers: {
                                'X-XSRF-Token': $('meta[name="_token"]').attr('content')
                            },
                            success: function (html) {
                                location.reload();
                            }
                        });
                    }
                });
            }

            function rejectOffer(nm,ID){
                bootbox.confirm("Are you sure you want to REJECT offer from \"" + nm + "\" ? ", function(result) {
                    if(result) {

                        $.ajax({
                            url: '/profile/donor_requests/reject/' + ID,
                            type: 'get',
                            headers: {
                                'X-XSRF-Token': $('meta[name="_token"]').attr('content')
                            },
                            success: function (html) {
                                location.reload();
                            }
                        });
                    }
                });
            }

        </script>
    @endsection





