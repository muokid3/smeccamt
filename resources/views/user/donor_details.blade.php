


    <div class="container-fluid">
        <div class="row">
            <h3 class="text-center">
                {{Auth::user()->bus_name}}
            </h3>
            <h5 class="text-center">Donor Details</h5>
            <hr/>

            <h2 style="text-align: center; margin-bottom: 2%">Donor Profile - {{$donor->name}}</h2>

            <div class="col-md-12">

                <!-- Nav tabs -->
                <div class="card">

                    <!-- Tab panes -->
                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane active" id="add-purchase-order">
                            <div class="row">
                                <div class="column">
                                    <div class="col-sm-6 col-md-6 col-xs-12">

                                        <div class="col-md-12">
                                            <h4><span style="color: red; ">Company Details</span> </h4>
                                            <hr/>
                                        </div>

                                        <div class="col-md-12">
                                            <label>Name: </label>
                                            {{$donor->name}}

                                        </div>

                                        <div class="col-md-12">
                                            <label>Type of Business:</label>
                                            {{$donor->type->business_type}}

                                        </div>

                                        <div class="col-md-12">
                                            <label>Industry: </label>
                                            {{$donor->industry->industry}}

                                        </div>


                                        <div class="col-md-12">
                                            <label for="unit_size">Location of Operation: </label>
                                            {{$donor->physical_location}}

                                        </div>

                                        <div class="col-md-12">
                                            <label for="unit_size">Total Donations Completed: </label>
                                            {{$donor->total_donations($donor->id)}}

                                        </div>



                                        {{--@if(Auth::user()->entity_id == $order->entity_id)--}}

                                            {{--<div style="margin-top: 2%" class="col-md-12">--}}
                                                {{--<h4><span style="color: red; ">Bids ({{$order->bids()}})</span> </h4>--}}
                                                {{--<hr/>--}}
                                            {{--</div>--}}


                                        {{--@else--}}
                                            {{--<div style="margin-top: 2%" class="col-md-12">--}}
                                                {{--<h4><span style="color: red; ">Client Details</span> </h4>--}}
                                                {{--<hr/>--}}
                                            {{--</div>--}}
                                            {{--<div class="col-md-12">--}}
                                                {{--<label>Client Name: </label>--}}
                                                {{--{{$order->entity->name}}--}}
                                            {{--</div>--}}

                                            {{--<div class="col-md-12">--}}
                                                {{--<label>Business Phone: </label>--}}
                                                {{--{{$order->entity->phone_no}}--}}
                                            {{--</div>--}}
                                            {{--<div class="col-md-12">--}}
                                                {{--<label>E-Mail: </label>--}}
                                                {{--{{$order->entity->email}}--}}
                                            {{--</div>--}}

                                            {{--<div class="col-md-12">--}}
                                                {{--<label>Physical Location: </label>--}}
                                                {{--{{$order->entity->physical_location}}--}}
                                            {{--</div>--}}
                                        {{--@endif--}}



                                    </div>

                                </div>
                                <div class="column">
                                    <div class="col-sm-6 col-md-6 hidden-xs" style="width: 50%; ">

                                        <div class="col-md-12">
                                            <h4><span style="color: red; ">Contact Details</span> </h4>
                                            <hr/>
                                        </div>

                                        <div class="col-md-12">
                                            <label>Postal Address: </label>
                                            {{$donor->postal_address}}

                                        </div>

                                        <div class="col-md-12">
                                            <label>Phone Number:</label>
                                            {{$donor->phone_no}}

                                        </div>

                                        <div class="col-md-12">
                                            <label>E-Mail: </label>
                                            {{$donor->email}}

                                        </div>

                                    </div>
                                </div>

                            </div>

                            <div class="row" style="margin-top: 2%">
                                    <div class="col-md-12">
                                        <h4><span style="color: red; ">Terms of Operation</span> </h4>
                                        <hr/>
                                    </div>
                                    <div class="col-md-12">

                                        {{$donor->terms}}

                                    </div>
                            </div>

                            <div class="row" style="margin-top: 2%">
                                    <div class="col-md-12">
                                        <h4><span style="color: red; ">Donation History</span> </h4>
                                        <h5><i>Donation history between you and <strong><u>{{$donor->name}}</u></strong></i></h5>
                                        <hr/>

                                        <table class="table table-striped table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Donation Type</th>
                                                <th>Product</th>
                                                <th>Amount/Size</th>
                                                <th>Donation Date</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach($donations as $donation)

                                                <tr>

                                                    <td>
                                                        {{$donation->type}}
                                                    </td>
                                                    <td>
                                                        {{$donation->donation_request->product->product_name}}
                                                    </td>

                                                    <td>
                                                        {{$donation->size}}
                                                    </td>


                                                    <td class="hidden-xs">
                                                        {{\Carbon\Carbon::parse(date("d-m-Y", strtotime($donation->updated_at)))
                                                                                                  ->formatLocalized('%A %d %B %Y')}}
                                                    </td>


                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>

                                        {{$donations->links()}}

                                    </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>

