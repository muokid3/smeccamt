


    <div class="container-fluid">
        <div class="row">

                <h3 class="text-center">
                    {{Auth::user()->bus_name}}
                </h3>
                <h5 class="text-center">You can create and manage your stock from here </h5>
            <hr/>

            <div style="margin-top: 5%" class="col-md-12">
                <!-- Nav tabs --><div class="card">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#stock" aria-controls="stock" role="tab" data-toggle="tab">Current Stock</a></li>
                        <li role="presentation"><a href="#add-stock" aria-controls="add-stock" role="tab" data-toggle="tab">Add Stock</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="stock">
                            <table class="table table-striped table-responsive">
                                <thead>
                                <tr>
                                    <th>Product Name</th>
                                    <th>Unit Size</th>
                                    <th>Available Units</th>
                                    <th>Date Created</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($stocks as $stock)

                                    <tr>

                                        <td>
                                            {{$stock->product->product_name}}
                                        </td>
                                        <td>
                                            {{$stock->unit_size}}
                                        </td>

                                        <td>
                                            {{$stock->units}}
                                        </td>

                                        <td>
                                            {{\Carbon\Carbon::parse(date("d-m-Y", strtotime($stock->created_at)))
                                                                                      ->formatLocalized('%A %d %B %Y')}}
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            {{$stocks->links()}}
                        </div>

                        <div role="tabpanel" class="tab-pane" id="add-stock">
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('/profile/stock/add_stock') }}">
                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-sm-6">
                                            <label>Select Product</label>
                                            {!! Form::select('product', \App\Product::where('entity_id', Auth::user()->entity_id)->pluck('product_name', 'id'), "",
                                            ['class' => 'select2 form-control', 'id'=>'product']) !!}
                                            <div style="color: red">{{$errors->first("product") }}</div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-md-6">

                                            <label for="unit_size">Unit Size</label>
                                            {!! Form::text('unit_size', '',['class'=>'form-control','id'=>'unit_size','placeholder'=>'eg. 1kg Rice']) !!}

                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-md-6">
                                        <label for="units">Number of Units</label>
                                        {!! Form::number('units', '',['class'=>'form-control','id'=>'units','min'=>'0','placeholder'=>'eg. 50']) !!}

                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-6" style="margin-top: 2%">
                                        <input type="submit" value="Create" class="btn btn-block btn-primary align-left">
                                    </div>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div>


        </div>

    </div>

