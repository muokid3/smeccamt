


    <div class="container-fluid">
        <div class="row">

                <h3 class="text-center">
                    {{Auth::user()->bus_name}}
                </h3>
                <h5 class="text-center">
                    Request for a donor! Publish your request for the donation request of your choice and wait for a suitable donor
                    to contact you!
                </h5>
            <hr/>


            <div style="margin-top: 1%" class="col-md-12">
                <!-- Nav tabs --><div class="card">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#products" aria-controls="products" role="tab" data-toggle="tab">My Requests</a></li>
                        <li role="presentation"><a href="#add-request" aria-controls="add-request" role="tab" data-toggle="tab">Add New Request</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="products">
                            <h4>Here is a list of request you have made</h4>
                            <table class="table table-striped table-responsive">
                                <thead>
                                <tr>
                                    <th>Request Type</th>
                                    <th>Size/Amount</th>
                                    <th>Product</th>
                                    <th>Description</th>
                                    <th>Offers</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($requests as $request)

                                    <tr>

                                        <td>
                                            {{$request->type}}
                                        </td>
                                        <td>
                                            {{$request->size}}
                                        </td>

                                        <td>
                                            {{$request->product->product_name}}
                                        </td>

                                        <td>
                                            {{$request->description}}
                                        </td>

                                        <td>
                                            {{\App\Donation::where('request_id',$request->id)->count()}}
                                        </td>
                                        <td>
                                            <a href="{{url('/profile/donor_requests/'.$request->id)}}">
                                                <i class="fa fa-eye" style="color: blue">View</i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            {{$requests->links()}}
                        </div>

                        <div role="tabpanel" class="tab-pane" id="add-request">
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('/profile/donor_requests/new') }}">
                                {{ csrf_field() }}

                                <div class="row">
                                        <div class="col-sm-6 col-md-6 col-xs-12">

                                            <div class="col-md-12">
                                                <label>Request Type</label>
                                                    <select name="type" class="select2 form-control">
                                                        <option value="cash">Cash</option>
                                                        <option value="Goods/Services">Goods/Services</option>
                                                    </select>
                                            </div>

                                            <div class="col-md-12">
                                                <label>Select Product</label>
                                                {!! Form::select('product', \App\Product::where('entity_id', Auth::user()->entity_id)->pluck('product_name', 'id'), "",
                                                ['class' => 'select2 form-control', 'id'=>'product']) !!}
                                                <div style="color: red">{{$errors->first("product") }}</div>
                                            </div>


                                            <div class="col-md-12">
                                                <label>Size/Amount</label>
                                                <input type="text" name="size" required value="{{old('size')}}" class="form-control">
                                                <div style="color: red">{{$errors->first("size") }}</div>

                                            </div>


                                            <div class="col-sm-12">
                                                    <label>Description (Please justify your request briefly)</label>
                                                    <textarea name="description" class="form-control" required placeholder="" rows="6">{{old('description')}}</textarea>
                                                    <div style="color: red">{{$errors->first("description") }}</div>
                                            </div>

                                        </div>


                                </div>

                                <div class="col-md-6" style="margin-top: 2%">
                                    <input type="submit" value="Create" class="btn btn-block btn-primary align-left">
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

