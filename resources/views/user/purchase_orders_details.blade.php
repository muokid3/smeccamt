


    <div class="container-fluid">
        <div class="row">

                <h3 class="text-center">
                    {{Auth::user()->bus_name}}
                </h3>
                <h5 class="text-center">Purchase Order Details </h5>
            <hr/>

            <h2 style="text-align: center; margin-bottom: 2%">{{$order->product->product_name}} - Proforma Purchase Order Details</h2>

            <div class="col-md-12">

                <!-- Nav tabs -->
                <div class="card">

                    <!-- Tab panes -->
                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane active" id="add-purchase-order">
                                <div class="row">
                                    <input type="hidden" id="lat" name="lat">
                                    <input type="hidden" id="lng" name="lng">
                                    <div class="column">
                                        <div class="col-sm-6 col-md-6 col-xs-12">

                                            <div class="col-md-12">
                                                <h4><span style="color: red; ">Order Details</span> </h4>
                                                <hr/>
                                            </div>

                                            <div class="col-md-12">
                                                <label>Product: </label>
                                                {{$order->product->product_name}} - ({{$order->unit_size}})

                                            </div>

                                            <div class="col-md-12">
                                                <label>Units Required:</label>
                                                {{$order->units}}

                                            </div>

                                            <div class="col-md-12">
                                                <label>Location</label>
                                                {{$order->location}}

                                            </div>


                                            <div class="col-md-12">
                                                <label for="unit_size">Delivery: </label>
                                                {{$order->delivery ? 'Delivery is required' : 'Delivery is not required'}}

                                            </div>


                                            <div class="col-md-12">
                                                <label for="units">Date Created: </label>
                                                {{\Carbon\Carbon::parse(date("d-m-Y", strtotime($order->created_at)))
                                                                                      ->formatLocalized('%A %d %B %Y')}}
                                            </div>


                                            @if(Auth::user()->entity_id == $order->entity_id)

                                                <div style="margin-top: 2%" class="col-md-12">
                                                    <h4><span style="color: red; ">Bids ({{$order->bids()}})</span> </h4>
                                                    <hr/>
                                                </div>

                                                <table class="table table-striped table-responsive">
                                                    <thead>
                                                    <tr>
                                                        <th>Bidder</th>
                                                        <th>Available Units</th>
                                                        <th class="hidden-xs">Unit Price</th>
                                                        <th>Total Price</th>
                                                        <th class="hidden-xs">Payment Mode</th>
                                                        <th class="hidden-xs">Delivery/Collection Date</th>
                                                        <th></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    @foreach(\App\Bid::where('purchase_order_id','=',$order->id)->orderBy('id', 'desc')->get() as $bid)

                                                        <tr>

                                                            <td>
                                                                {{$bid->bidder->name}}
                                                            </td>
                                                            <td>
                                                                {{$bid->units}}
                                                            </td>

                                                            <td class="hidden-xs">
                                                                {{$bid->unit_price}}
                                                            </td>

                                                            <td>
                                                                {{$bid->total_price}}
                                                            </td>

                                                            <td class="hidden-xs">
                                                                {{$bid->payment_mode->mode}}
                                                            </td>


                                                            <td class="hidden-xs">
                                                                {{\Carbon\Carbon::parse(date("d-m-Y", strtotime($bid->dispatch_date)))
                                                                                                          ->formatLocalized('%A %d %B %Y')}}
                                                            </td>

                                                            <td>
                                                                @if($bid->status == 1)
                                                                    <a href="javascript:void(0);" onclick="approve('{{$bid->bidder->name}}','{{$bid->id}}');">
                                                                        <span class="label label-primary">
                                                                            Approve
                                                                        </span>
                                                                    </a>

                                                                    <a href="javascript:void(0);" onclick="rejectbid('{{$bid->bidder->name}}','{{$bid->id}}');">
                                                                        <span class="label label-danger">
                                                                            Reject
                                                                        </span>
                                                                    </a>
                                                                @elseif($bid->status == 2)
                                                                    <span class="label label-success">{{$bid->bid_status()}}</span>
                                                                @else
                                                                    <span class="label label-warning">{{$bid->bid_status()}}</span>
                                                                @endif


                                                            </td>

                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>

                                            @else
                                                <div style="margin-top: 2%" class="col-md-12">
                                                    <h4><span style="color: red; ">Client Details</span> </h4>
                                                    <hr/>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Client Name: </label>
                                                    {{$order->entity->name}}
                                                </div>

                                                <div class="col-md-12">
                                                    <label>Business Phone: </label>
                                                    {{$order->entity->phone_no}}
                                                </div>
                                                <div class="col-md-12">
                                                    <label>E-Mail: </label>
                                                    {{$order->entity->email}}
                                                </div>

                                                <div class="col-md-12">
                                                    <label>Physical Location: </label>
                                                    {{$order->entity->physical_location}}
                                                </div>
                                            @endif



                                        </div>

                                    </div>
                                    <div class="column">
                                        <div class="col-sm-6 col-md-6 hidden-xs" style="height: 500px;width: 50%; ">

                                            <div id="mapcanvas"></div>

                                        </div>
                                    </div>


                                </div>

                                @if(Auth::user()->entity_id != $order->entity_id)
                                    @if(\App\Bid::where('purchase_order_id','=', $order->id)->where('bidder_entity_id','=',Auth::user()->entity_id)->first())
                                        <div class="col-md-6">
                                            <label style="color: red;">You have already placed a bid for this purchase order. </label>
                                            <a href="{{url('profile/bid/'.\App\Bid::where('purchase_order_id','=', $order->id)
                                                                ->where('bidder_entity_id','=',Auth::user()->entity_id)
                                                                    ->first()->id)}}">View Bid</a>
                                        </div>
                                    @else
                                        <div class="col-md-6" style="margin-top: 2%">
                                            {{--<input type="submit" value="Place a Bid" class="btn btn-block btn-primary align-left">--}}
                                            <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#bidModal">
                                                Place a Bid
                                            </button>
                                        </div>
                                    @endif

                                @endif

                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>



    @section('css')
        <style>
            /* Always set the map height explicitly to define the size of the div
             * element that contains the map. */
            #mapcanvas {
                height: 100%;
                width: 100%;
            }

        </style>
    @endsection

    @section("scripts")
        <script>


            function initMap() {

                var myLatLng = new google.maps.LatLng(<?php echo $order->latitude . ',' . $order->longitude; ?>);


                var map = new google.maps.Map(document.getElementById('mapcanvas'), {
                    zoom: 15,
                    center: myLatLng
                });

                var marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    title: 'Hello World!'
                });

                var opCircle = new google.maps.Circle({
                    strokeColor: '#4CAF50',
                    strokeOpacity: 0.8,
                    strokeWeight: 3,
                    fillColor: '#DCEDC8',
                    fillOpacity: 0.7,
                    map: map,
                    center: myLatLng,
                    radius: 1000
                });
            }




            function approve(nm,bidID){
                bootbox.confirm("Are you sure you want to APPROVE bid from \"" + nm + "\" ? ", function(result) {
                    if(result) {

                        $.ajax({
                            url: '/profile/bid/approve/' + bidID,
                            type: 'get',
                            headers: {
                                'X-XSRF-Token': $('meta[name="_token"]').attr('content')
                            },
                            success: function (html) {
                                location.reload();
                            }
                        });
                    }
                });
            }

            function rejectbid(nm,bidID){
                bootbox.confirm("Are you sure you want to REJECT bid from \"" + nm + "\" ? ", function(result) {
                    if(result) {

                        $.ajax({
                            url: '/profile/bid/reject/' + bidID,
                            type: 'get',
                            headers: {
                                'X-XSRF-Token': $('meta[name="_token"]').attr('content')
                            },
                            success: function (html) {
                                location.reload();
                            }
                        });
                    }
                });
            }


        </script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA50nWlMzxA5OEFPJLoba4oAGvGI40k6Jc&callback=initMap"></script>


    @endsection



