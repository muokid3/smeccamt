


    <div class="container-fluid">
        <div class="row">

                <h3 class="text-center">
                    {{Auth::user()->bus_name}}
                </h3>
                <h5 class="text-center">
                    Find appeals for donations and make a donation to a request of your choice
                </h5>
            <hr/>


            <div style="margin-top: 1%" class="col-md-12">
                <!-- Nav tabs --><div class="card">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#products" aria-controls="products" role="tab" data-toggle="tab">Donor Appeal Requests</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="products">
                            <table class="table table-striped table-responsive">
                                <thead>
                                <tr>
                                    <th>Entity</th>
                                    <th>Request Type</th>
                                    <th>Size/Amount</th>
                                    <th>Product</th>
                                    <th>Description</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($requests as $donorrequest)

                                    <tr>
                                        <td>
                                            {{\App\Entity::find($donorrequest->entity_id)->name}}
                                        </td>

                                         <td>
                                            {{$donorrequest->type}}
                                        </td>

                                        <td>
                                            {{$donorrequest->size}}
                                        </td>

                                        <td>
                                            {{$donorrequest->product->product_name}}
                                        </td>

                                        <td>
                                            {{$donorrequest->description}}
                                        </td>

                                        <td>
                                            <a href="{{url('profile/donor_appeals/'.$donorrequest->id)}}">
                                                View/Donate
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            {{$requests->links()}}
                        </div>
                    </div>
                </div>
            </div>


        </div>

    </div>



