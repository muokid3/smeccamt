


    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12  col-sm-12 col-xs-12 ">

                <div class="alert alert-success">

                    Welcome to SMECCA! Congratulations, you made it so far! Please tell us more about your business to continue...
                </div>

            </div>



                        <!--      Wizard container        -->
                        <div class="wizard-container">
                            <div class="card wizard-card" data-color="green" id="wizard">
                                <form action="{!! url('/create_entity') !!}" method="post">
                                    {!! csrf_field() !!}
                                    <!--        You can switch " data-color="blue" "  with one of the next bright colors: "green", "orange", "red", "purple"             -->

                                    <div class="wizard-header">
                                        <h3 class="wizard-title">
                                            {{Auth::user()->bus_name}}
                                        </h3>
                                        <h5>Set up your business on SMECCA. Tell us about what you do</h5>
                                    </div>
                                    <div class="wizard-navigation">
                                        <ul>
                                            <li><a href="#details" data-toggle="tab">Basic Info</a></li>
                                            <li><a href="#captain" data-toggle="tab">Contact Info</a></li>
                                            <li><a href="#description" data-toggle="tab">My Terms</a></li>
                                        </ul>
                                    </div>

                                    <div class="tab-content">
                                        <div class="tab-pane" id="details">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h4 class="info-text"> Let's start with the basic details.</h4>
                                                </div>
                                                <div class="col-sm-12">

                                                    <div class="form-group label-floating" style="margin-top: 0px">
                                                        <input type="text" name="location" required id="locationOfOperation" autofocus value="{{old('location')}}" class="form-control">
                                                        <div style="color: red">{{$errors->first("location") }}</div>

                                                    </div>

                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group " style="margin-top: 0px">
                                                        <label class="control-label">Select Industry</label>
                                                        {!! Form::select('industry', \App\Industry::pluck('industry', 'id'), "",
                                                        ['class' => 'select2 form-control', 'id'=>'select-industry']) !!}
                                                        <div style="color: red">{{$errors->first("industry") }}</div>

                                                    </div>

                                                </div>

                                                <div class="col-sm-6">

                                                    <div class="form-group " style="margin-top: 0px">
                                                        <label class="control-label">Type of Business</label>
                                                        {!! Form::select('business_type', \App\BusinessType::pluck('business_type', 'id'), "",
                                                        ['class' => 'select2 form-control', 'id'=>'business_type']) !!}
                                                        {{$errors->first("business_type") }}
                                                        <div style="color: red">{{$errors->first("business_type") }}</div>

                                                    </div>

                                                </div>


                                                <div class="col-sm-12">
                                                    <div class="form-group" style="margin-top: 0px">
                                                        <label class="control-label">Select line of business (All that Apply)</label>
                                                        {!! Form::select('lob[]', \App\Lob::pluck('lob', 'id'), "",
                                                        ['class' => 'select2 form-control', 'id'=>'select-lob', 'multiple']) !!}
                                                        <div style="color: red">{{$errors->first("lob") }}</div>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                        <div class="tab-pane" id="captain">
                                            <h4 class="info-text">Please provide means of reaching you and your business/organization</h4>
                                            <div class="row">

                                                <h5 style="padding-left: 30px" class="control-label">Location</h5>


                                                <div class="col-sm-12 ">
                                                    <div class="col-sm-6">
                                                        <div class="form-group label-floating" style="margin-top: 0px">
                                                            <label class="control-label">Physical Location </label>
                                                            <input type="text" name="physical_location" value="{{old('physical_location')}}" class="form-control">
                                                            <div style="color: red">{{$errors->first("physical_location") }}</div>

                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group label-floating" style="margin-top: 0px">
                                                            <label class="control-label">Postal Address</label>
                                                            <input type="text" name="postal_address" value="{{old('postal_address')}}" class="form-control">
                                                            <div style="color: red">{{$errors->first("postal_address") }}</div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <h5 style="padding-left: 30px" class="control-label">Contact Information </h5>


                                                <div class="col-sm-12">
                                                    <div class="col-sm-6">
                                                        <div class="form-group label-floating" style="margin-top: 0px">
                                                            <label class="control-label">Phone Number </label>
                                                            <input type="number" name="phone_no"  value="{{old('phone_no')}}" class="form-control">
                                                            <div style="color: red">{{$errors->first("phone_no") }}</div>

                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group label-floating" style="margin-top: 0px">
                                                            <label class="control-label">Business E-Mail Address</label>
                                                            <input type="email" name="email_address" value="{{old('email_address')}}" class="form-control">
                                                            <div style="color: red">{{$errors->first("email_address") }}</div>

                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="tab-pane" id="description">
                                            <div class="row">
                                                <h4 class="info-text"> What are your terms of operation with other businesses?</h4>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label>Terms</label>
                                                        <textarea name="terms" class="form-control" placeholder="" rows="6">{{old('terms')}}</textarea>
                                                        <div style="color: red">{{$errors->first("terms") }}</div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wizard-footer">
                                        <div class="pull-right">
                                            <input type='button' class='btn btn-next btn-fill btn-success btn-wd' name='next' value='Next' />
                                            <input type='submit' class='btn btn-finish btn-fill btn-success btn-wd' name='finish' value='Finish' />
                                        </div>
                                        <div class="pull-left">
                                            <input type='button' class='btn btn-previous btn-fill btn-default btn-wd' name='previous' value='Previous' />


                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </form>
                            </div>
                        </div> <!-- wizard container -->





        </div>

    </div>



@section("scripts")
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA50nWlMzxA5OEFPJLoba4oAGvGI40k6Jc&libraries=places"></script>
    <script>
        var autocomplete;
        var input = document.getElementById('locationOfOperation');
        var options = {
            //types: ['(cities)'],
            componentRestrictions: {country: 'ke'}
        };

        autocomplete = new google.maps.places.Autocomplete(input, options);

        $(document).ready(function (e) {
            var industry = $("#select-industry");
            var lob = $("#select-lob");


            $.ajax("{{url('/industry_lobs')}}/" + industry.val(), {
                success: function (message) {
                    console.log(message);
                    var temp = JSON.parse(message);
                    var listItems = "<option value='' disabled>--Select line of business (All that Apply)--</option>";
                    $.each(temp, function (i, item) {
                        listItems += '<option value=' + temp[i].id + '>' + temp[i].lob + '</option>';
                    });
                    lob.html(listItems);
                    lob.attr("disabled", false);
                },
                error: function (error) {
                    console.log(error);
                }
            });



            industry.on('change', function () {
                lob.empty();
                lob.html('');
                $.ajax("{{url('/industry_lobs')}}/" + industry.val(), {
                    success: function (message) {
                        console.log(message);
                        var temp = JSON.parse(message);
                        var listItems = "<option value='' disabled>--Select line of business (All that Apply)--</option>";
                        $.each(temp, function (i, item) {
                            listItems += '<option value=' + temp[i].id + '>' + temp[i].lob + '</option>';
                        });
                        lob.html(listItems);
                        lob.attr("disabled", false);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            });
        });


    </script>
    <script src="{{ url('/wizard/js/jquery.bootstrap.js')}}"></script>
    <script src="{{ url('/wizard/js/material-bootstrap-wizard.js')}}"></script>
    <script src="{{ url('/wizard/js/jquery.validate.min.js')}}"></script>
    <link rel="stylesheet" href="{{ url('/wizard/css/material-bootstrap-wizard.css') }}">




@endsection


