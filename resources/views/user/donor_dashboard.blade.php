


    <div class="container-fluid">
        <div class="row">

                <h3 class="text-center">
                    {{Auth::user()->bus_name}}
                </h3>
                <h5 class="text-center">You are registered as on SMECCA as: {{Auth::user()->type->name}} </h5>
            <hr/>

            <div class="container-fluid">


                <div class="col-md-3">

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-6">
                                    <i class="fa fa-list-ol fa-5x"></i>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <p class="announcement-heading">4</p>
                                    <p class="announcement-text">Customers</p>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer announcement-bottom">
                                <div class="row">
                                    <div class="col-xs-6">
                                        View
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>

                </div>


                <div class="col-md-3">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-6">
                                    <i class="fa fa-usd fa-5x"></i>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <p class="announcement-heading"> R$ 950 mil</p>
                                    <p class="announcement-text">Revenue</p>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer announcement-bottom">
                                <div class="row">
                                    <div class="col-xs-6">
                                        Details
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>

                </div>



                <div class="col-md-3">

                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-6">
                                    <i class="fa fa-line-chart fa-5x"></i>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <p class="announcement-heading"> 2,3 months</p>
                                    <p class="announcement-text">Average time</p>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer announcement-bottom">
                                <div class="row">
                                    <div class="col-xs-6">
                                        Details
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>

                </div>



                <div class="col-md-3">

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-6">
                                    <i class="fa fa-money fa-5x"></i>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <p class="announcement-heading"> $ 250 k</p>
                                    <p class="announcement-text">Recovered <i class=""></i>  </p>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer announcement-bottom">
                                <div class="row">
                                    <div class="col-xs-6">
                                        Details
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>

                </div>




                <div class="col-md-6">

                    <h3>Top Suppliers</h3>

                    <table class="table table-condensed table-bordered table-hover small">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Name</th>
                            <th>Revenue</th>
                            <th>Tax</th>
                            <th>Cap</th>
                            <th>ROI</th>
                            <th>Interest</th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <td>
                                <a href="#"> <i class="fa fa-dashboard"></i></a>
                            </td>
                            <td>
                                Company 1
                            </td>
                            <td>
                                $ 350 k
                            </td>
                            <td>
                                4
                            </td>
                            <td>
                                $ 291,75
                            </td>
                            <td>3,72</td>
                            <td>
                                3,65%
                            </td>

                        </tr>

                        <tr class="danger">
                            <td>
                                <a href="#"> <i class="fa fa-dashboard"></i></a>

                            </td>

                            <td>
                                Customer 2
                            </td>
                            <td>$ 270 k</td>
                            <td>7,8 </td>
                            <td>$ 1307.32</td>
                            <td>2,61</td>
                            <td>8,45%</td>
                        </tr>

                        <tr class="info">
                            <td>
                                <a href="#"> <i class="fa fa-dashboard"></i></a>

                            </td>

                            <td>
                                Customer 3
                            </td>
                            <td>$ 125 k</td>
                            <td>2,5 </td>
                            <td>$ 312.45</td>
                            <td>9,21</td>
                            <td>2,5%</td>
                        </tr>

                        <tr >
                            <td>
                                <a href="#"> <i class="fa fa-dashboard"></i></a>

                            </td>

                            <td>
                                Customer 4
                            </td>
                            <td>$ 205 k</td>
                            <td>7,1 </td>
                            <td>$ 115,75</td>
                            <td>6,27</td>
                            <td>(n/d)</td>
                        </tr>


                        </tbody>

                    </table>

                </div>

                <div class="col-md-6">
                    <div id='grafico1' class="grafico"></div>
                </div>

                <div class="col-md-6">
                    <div id='grafico2' class="grafico"></div>
                </div>
                <div class="col-md-6">
                    <div id='grafico3' class="grafico"></div>
                </div>




                <div class="col-md-8">

                    <h3>Messages</h3>

                    <table class="table table-condensed table-bordered table-hover small">
                        <thead>
                        <tr>
                            <th style="width:30px;"></th>
                            <th>From</th>
                            <th>Subject</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>




                        <tr class="text-info">
                            <td>
                                <i class="fa fa-info"></i>
                            </td>
                            <td>
                                John Doe
                            </td>
                            <td>
                                Complaint
                            </td>
                            <td>
                                <a href="#"> <i class="fa fa-eye"></i></a>
                            </td>

                        </tr>


                        <tr class="text-info">
                            <td>
                                <i class="fa fa-info"></i>
                            </td>
                            <td>
                                Mr Joe
                            </td>
                            <td>
                                Congrats
                            </td>
                            <td>
                                <a href="#"> <i class="fa fa-eye"></i></a>
                            </td>

                        </tr>



                        </tbody>

                    </table>

                </div>




            </div>


        </div>
    </div>




    @section('scripts')
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/highcharts-more.js"></script>

        <script>

            document.addEventListener("DOMContentLoaded", function(event) {

                Highcharts.chart('grafico1', {

                    credits: {
                        enabled: false
                    },
                    title: {
                        text: 'Revenue by brand'
                    },


                    yAxis: {
                        title: {
                            text: ' $'
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },

                    plotOptions: {
                        series: {
                            pointStart: 1
                        }
                    },

                    series: [{
                        name: 'Brand 1',
                        data: [23, 27, 32, 26, 25, 27, 22, 19]
                    }, {
                        name: 'Brand 2',
                        data: [45, 49, 43, 41, 42, 41, 38, 34]
                    }, {
                        name: 'Brand 3',
                        data: [43, 58, 51, 52, 51, 60, 62, 65]
                    }, {
                        name: 'Brand 4',
                        data: [null, null, 23, 15, 19, 21, 27, 30]
                    }]

                });









                Highcharts.chart('grafico2', {
                    chart: {
                        type: 'areaspline'
                    },

                    credits: {
                        enabled: false
                    },
                    title: {
                        text: 'Forecast - ROI'
                    },


                    yAxis: {
                        title: {
                            text: 'Months'
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },

                    plotOptions: {
                        series: {
                            pointStart: 1
                        }
                    },

                    series: [{
                        name: 'Product 1',
                        data: [15, 27, 32, 26, 25, 20]
                    }, {
                        name: 'Product 2',
                        data: [43, 41, 42, 41, 38, 34]
                    }, {
                        name: 'Product 3',
                        data: [51, 52, 51, 60, 62, 65]
                    }, {
                        name: 'Product 4',
                        data: [null, null, 29, 22, 19, 21]
                    }]

                });




                Highcharts.chart('grafico3', {
                    chart: {
                        type: 'column'
                    },

                    credits: {
                        enabled: false
                    },
                    title: {
                        text: 'Runway overview'
                    },
                    xAxis: {
                        categories: ['Product 1', 'Product 2', 'Product 3', 'Product 4']
                    },
                    yAxis: {
                        title: {
                            text: 'value (USD)'
                        },
                        stackLabels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold',
                                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                            }
                        }
                    },
                    legend: {
                        align: 'center',
                        verticalAlign: 'bottom',
                        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                        borderColor: '#CCC',
                        shadow: false
                    },
                    tooltip: {
                        headerFormat: '<b>{point.x}</b><br/>',
                        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
                    },
                    plotOptions: {
                        column: {
                            stacking: 'normal',
                            dataLabels: {
                                enabled: false,
                                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                            }
                        }
                    },
                    series: [{
                        name: 'Debit',
                        data: [0,0,0,15],
                        color : '#FA5858'

                    }, {
                        name: 'Available',
                        data: [150, 130, 25,  0],
                        color : '#58FA58'
                    }, {
                        name: 'Used',
                        data: [100, 140, 100, 205],
                        color : '#FE9A2E'

                    }]
                });




            });

        </script>
    @endsection

    @section('css')

        <style>
            .grafico {
                min-width: 310px;
                max-width: 400px;
                height: 280px;
                margin: 0 auto
            }

            .main-header {
                font-size: x-large;
                color : #888;
                font-family: Verdana;
                margin-bottom: 20px;
            }

            .destaque {
                color: #f88;
                font-weight: bolder;
            }

            .highcharts-tooltip h3 {
                margin: 0.3em 0;
            }
        </style>
    @endsection

