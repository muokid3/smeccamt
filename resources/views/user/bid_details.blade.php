


    <div class="container-fluid">
        <div class="row">

                <h3 class="text-center">
                    {{Auth::user()->bus_name}}
                </h3>
                <h5 class="text-center">Purchase Order Details </h5>
            <hr/>

            <h2 style="text-align: center; margin-bottom: 2%">{{$order->product->product_name}} - Proforma Purchase Order Details</h2>

            <div class="col-md-12">

                <!-- Nav tabs -->
                <div class="card">

                    <!-- Tab panes -->
                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane active" id="add-purchase-order">
                                <div class="row">
                                    <input type="hidden" id="lat" name="lat">
                                    <input type="hidden" id="lng" name="lng">
                                    <div class="column">
                                        <div class="col-sm-6 col-md-6 col-xs-12">

                                            <div class="col-md-12">
                                                <h4><span style="color: red; ">Order Details</span> </h4>
                                                <hr/>
                                            </div>

                                            <div class="col-md-6">
                                                <label>Product: </label>
                                                {{$order->product->product_name}} - ({{$order->unit_size}})

                                            </div>

                                            <div class="col-md-6">
                                                <label>Units Required:</label>
                                                {{$order->units}}

                                            </div>


                                            <div class="col-md-6">
                                                <label for="unit_size">Delivery: </label>
                                                {{$order->delivery ? 'Delivery is required' : 'Delivery is not required'}}

                                            </div>


                                            <div class="col-md-6">
                                                <label for="units">Date Created: </label>
                                                {{\Carbon\Carbon::parse(date("d-m-Y", strtotime($order->created_at)))
                                                                                      ->formatLocalized('%A %d %B %Y')}}
                                            </div>
                                            <div class="col-md-12">
                                                <label>Location</label>
                                                {{$order->location}}

                                            </div>


                                                <div style="margin-top: 2%" class="col-md-12">
                                                    <h4><span style="color: red; ">Client Details</span> </h4>
                                                    <hr/>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Client Name: </label>
                                                    {{$order->entity->name}}
                                                </div>

                                                <div class="col-md-12">
                                                    <label>Business Phone: </label>
                                                    {{$order->entity->phone_no}}
                                                </div>
                                                <div class="col-md-12">
                                                    <label>E-Mail: </label>
                                                    {{$order->entity->email}}
                                                </div>

                                                <div class="col-md-12">
                                                    <label>Physical Location: </label>
                                                    {{$order->entity->physical_location}}
                                                </div>

                                            <div style="margin-top: 2%" class="col-md-12">
                                                <h4><span style="color: red; ">Bid Details</span> </h4>
                                                <hr/>
                                            </div>
                                            <div class="col-md-12">
                                                <label>Bidder: </label>
                                                {{$bid->bidder->name}}
                                            </div>

                                            <div class="col-md-12">
                                                <label>Status: </label>
                                                {{$bid->bid_status()}}
                                            </div>

                                            <div class="col-md-6">
                                                <label>Supplied Units: </label>
                                                {{$bid->units}}
                                            </div>

                                            <div class="col-md-6">
                                                <label>Unit price (KSH): </label>
                                                {{$bid->unit_price}}
                                            </div>
                                            <div class="col-md-6">
                                                <label>Payment Mode: </label>
                                                {{$bid->payment_mode->mode}}
                                            </div>

                                            <div class="col-md-6">
                                                <label>Total Price (KSH): </label>
                                                {{$bid->total_price}}
                                            </div>



                                        </div>

                                    </div>
                                    <div class="column">
                                        <div class="col-sm-6 col-md-6 hidden-xs" style="height: 500px;width: 50%; ">

                                            <div id="mapcanvas"></div>

                                        </div>
                                    </div>


                                </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>



    @section('css')
        <style>
            /* Always set the map height explicitly to define the size of the div
             * element that contains the map. */
            #mapcanvas {
                height: 100%;
                width: 100%;
            }

        </style>
    @endsection

    @section("scripts")
        <script>


            function initMap() {

                var myLatLng = new google.maps.LatLng(<?php echo $order->latitude . ',' . $order->longitude; ?>);


                var map = new google.maps.Map(document.getElementById('mapcanvas'), {
                    zoom: 15,
                    center: myLatLng
                });

                var marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    title: 'Hello World!'
                });

                var opCircle = new google.maps.Circle({
                    strokeColor: '#4CAF50',
                    strokeOpacity: 0.8,
                    strokeWeight: 3,
                    fillColor: '#DCEDC8',
                    fillOpacity: 0.7,
                    map: map,
                    center: myLatLng,
                    radius: 1000
                });
            }





        </script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA50nWlMzxA5OEFPJLoba4oAGvGI40k6Jc&callback=initMap"></script>

    @endsection



