


    <div class="container-fluid">
        <div class="row">
            <h3 class="text-center">
                {{Auth::user()->bus_name}}
            </h3>
            <h5 class="text-center">Distributor Details</h5>
            <hr/>

            <h2 style="text-align: center; margin-bottom: 2%">Distributor Profile - {{$supplier->name}}</h2>

            <div class="col-md-12">

                <!-- Nav tabs -->
                <div class="card">

                    <!-- Tab panes -->
                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane active" id="add-purchase-order">
                            <div class="row">
                                <div class="column">
                                    <div class="col-sm-6 col-md-6 col-xs-12">

                                        <div class="col-md-12">
                                            <h4><span style="color: red; ">Company Details</span> </h4>
                                            <hr/>
                                        </div>

                                        <div class="col-md-12">
                                            <label>Name: </label>
                                            {{$supplier->name}}

                                        </div>

                                        <div class="col-md-12">
                                            <label>Type of Business:</label>
                                            {{$supplier->type->business_type}}

                                        </div>

                                        <div class="col-md-12">
                                            <label>Industry: </label>
                                            {{$supplier->industry->industry}}

                                        </div>


                                        <div class="col-md-12">
                                            <label for="unit_size">Location of Operation: </label>
                                            {{$supplier->physical_location}}

                                        </div>

                                        <div class="col-md-12">
                                            <label for="unit_size">Total Trades Completed: </label>
                                            {{$supplier->total_trades($supplier->id)}}

                                        </div>



                                        {{--@if(Auth::user()->entity_id == $order->entity_id)--}}

                                            {{--<div style="margin-top: 2%" class="col-md-12">--}}
                                                {{--<h4><span style="color: red; ">Bids ({{$order->bids()}})</span> </h4>--}}
                                                {{--<hr/>--}}
                                            {{--</div>--}}


                                        {{--@else--}}
                                            {{--<div style="margin-top: 2%" class="col-md-12">--}}
                                                {{--<h4><span style="color: red; ">Client Details</span> </h4>--}}
                                                {{--<hr/>--}}
                                            {{--</div>--}}
                                            {{--<div class="col-md-12">--}}
                                                {{--<label>Client Name: </label>--}}
                                                {{--{{$order->entity->name}}--}}
                                            {{--</div>--}}

                                            {{--<div class="col-md-12">--}}
                                                {{--<label>Business Phone: </label>--}}
                                                {{--{{$order->entity->phone_no}}--}}
                                            {{--</div>--}}
                                            {{--<div class="col-md-12">--}}
                                                {{--<label>E-Mail: </label>--}}
                                                {{--{{$order->entity->email}}--}}
                                            {{--</div>--}}

                                            {{--<div class="col-md-12">--}}
                                                {{--<label>Physical Location: </label>--}}
                                                {{--{{$order->entity->physical_location}}--}}
                                            {{--</div>--}}
                                        {{--@endif--}}



                                    </div>

                                </div>
                                <div class="column">
                                    <div class="col-sm-6 col-md-6 hidden-xs" style="width: 50%; ">

                                        <div class="col-md-12">
                                            <h4><span style="color: red; ">Contact Details</span> </h4>
                                            <hr/>
                                        </div>

                                        <div class="col-md-12">
                                            <label>Postal Address: </label>
                                            {{$supplier->postal_address}}

                                        </div>

                                        <div class="col-md-12">
                                            <label>Phone Number:</label>
                                            {{$supplier->phone_no}}

                                        </div>

                                        <div class="col-md-12">
                                            <label>E-Mail: </label>
                                            {{$supplier->email}}

                                        </div>

                                    </div>
                                </div>

                            </div>

                            <div class="row" style="margin-top: 2%">
                                    <div class="col-md-12">
                                        <h4><span style="color: red; ">Terms of Operation</span> </h4>
                                        <hr/>
                                    </div>
                                    <div class="col-md-12">

                                        {{$supplier->terms}}

                                    </div>
                            </div>

                            <div class="row" style="margin-top: 2%">
                                    <div class="col-md-12">
                                        <h4><span style="color: red; ">Trade History</span> </h4>
                                        <h5><i>Trade history between you and <strong><u>{{$supplier->name}}</u></strong></i></h5>
                                        <hr/>

                                        <table class="table table-striped table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Product</th>
                                                <th>Supplied Units</th>
                                                <th class="hidden-xs">Unit Price</th>
                                                <th>Total Price</th>
                                                <th class="hidden-xs">Payment Mode</th>
                                                <th class="hidden-xs">Delivery/Collection Date</th>
                                                <th>Status</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach($bids as $bid)

                                                <tr>

                                                    <td>
                                                        {{\App\Product::find($bid->product_id)->product_name}}
                                                    </td>
                                                    <td>
                                                        {{$bid->units}}
                                                    </td>

                                                    <td class="hidden-xs">
                                                        {{$bid->unit_price}}
                                                    </td>

                                                    <td>
                                                        {{$bid->total_price}}
                                                    </td>

                                                    <td class="hidden-xs">
                                                        {{$bid->payment_mode->mode}}
                                                    </td>


                                                    <td class="hidden-xs">
                                                        {{\Carbon\Carbon::parse(date("d-m-Y", strtotime($bid->dispatch_date)))
                                                                                                  ->formatLocalized('%A %d %B %Y')}}
                                                    </td>

                                                    <td>
                                                        {{$bid->bid_status()}}


                                                    </td>

                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>

                                        {{$bids->links()}}

                                    </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>

