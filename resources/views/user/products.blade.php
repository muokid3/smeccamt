


    <div class="container-fluid">
        <div class="row">

                <h3 class="text-center">
                    {{Auth::user()->bus_name}}
                </h3>
                <h5 class="text-center">A list of products/services that you deal with. Feel free to add all the products in your stock</h5>

            <hr/>


            <div style="margin-top: 5%" class="col-md-12">
                        <!-- Nav tabs --><div class="card">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#products" aria-controls="products" role="tab" data-toggle="tab">Products</a></li>
                                <li role="presentation"><a href="#add-product" aria-controls="add-product" role="tab" data-toggle="tab">Add Product</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="products">
                                    <table class="table table-striped table-responsive">
                                        <thead>
                                        <tr>
                                            <th>Product Name</th>
                                            <th>Product Code</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($products as $product)

                                            <tr>

                                                <td>
                                                    {{$product->product_name}}
                                                </td>
                                                <td>
                                                    {{$product->product_code}}
                                                </td>
                                                <td>
                                                    <a href="{{url('profile/products/'.$product->id)}}">
                                                        <i class="fa fa-edit" style="color: blue"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>

                                    {{$products->links()}}
                                </div>

                                <div role="tabpanel" class="tab-pane" id="add-product">
                                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/profile/products/create') }}">
                                        {{ csrf_field() }}
                                        <div class="col-md-6">
                                            <label for="product_name">Product Name</label>
                                            {!! Form::text('product_name', '',['class'=>'form-control','id'=>'product_name']) !!}

                                        </div>

                                        <div class="col-md-6">
                                            <label for="product_code">Product Code</label>
                                            {!! Form::text('product_code', '',['class'=>'form-control','id'=>'product_code']) !!}

                                        </div>

                                        <div class="col-md-12" style="margin-top: 2%">
                                            <input type="submit" value="Create" class="btn btn-primary">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
        </div>

    </div>

