


    <div class="container-fluid">
        <div class="row">

                <h3 class="text-center">
                    {{Auth::user()->bus_name}}
                </h3>
                <h5 class="text-center">Create your proforma purchase order to get supplies of your favorite goods/services </h5>
            <hr/>

            <div style="margin-top: 5%" class="col-md-12">
                <!-- Nav tabs --><div class="card">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#orders" aria-controls="orders" role="tab" data-toggle="tab">My Purchase Orders</a></li>
                        <li role="presentation"><a href="#add-purchase-order" aria-controls="add-purchase-order" role="tab" data-toggle="tab">Create New Purchase Orders</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="orders">
                            <table class="table table-striped table-responsive">
                                <thead>
                                <tr>
                                    <th>Product Name</th>
                                    <th>Unit Size</th>
                                    <th>Requested Units</th>
                                    <th>Bids</th>
                                    <th class="hidden-xs">Location</th>
                                    <th class="hidden-xs">Status</th>
                                    <th class="hidden-xs">Delivery Required</th>
                                    <th class="hidden-xs">Date Created</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($orders as $order)

                                    <tr>

                                        <td>
                                            {{$order->product->product_name}}
                                        </td>
                                        <td>
                                            {{$order->unit_size}}
                                        </td>

                                        <td>
                                            {{$order->units}}
                                        </td>

                                        <td>
                                            {{$order->bids()}} Bids
                                        </td>

                                        <td class="hidden-xs">
                                            {{$order->location}}
                                        </td>

                                        <td class="hidden-xs">
                                            {{$order->status ? 'Active' : 'Inactive'}}
                                        </td>

                                        <td class="hidden-xs">
                                            {{$order->delivery ? 'Yes' : 'No'}}
                                        </td>

                                        <td class="hidden-xs">
                                            {{\Carbon\Carbon::parse(date("d-m-Y", strtotime($order->created_at)))
                                                                                      ->formatLocalized('%A %d %B %Y')}}
                                        </td>

                                        <td>
                                            <a href="{{url('profile/purchase_orders/'.$order->id)}}"> <i class="fa fa-eye"></i> View Order</a>

                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            {{$orders->links()}}
                        </div>

                        <div role="tabpanel" class="tab-pane" id="add-purchase-order">
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('/profile/purchase_orders/create_purchase_order') }}">
                                {{ csrf_field() }}

                                <div class="row">

                                    <input type="hidden" id="lat" name="lat">
                                    <input type="hidden" id="lng" name="lng">
                                    <div class="column">
                                        <div class="col-sm-6 col-md-6 col-xs-12">

                                            <div class="col-md-12">
                                                <label>Line of Business</label>
                                                {!! Form::select('lob', \App\Lob::pluck('lob', 'id'), "",
                                                ['class' => 'select2 form-control', 'id'=>'lob']) !!}
                                                <div style="color: red">{{$errors->first("lob") }}</div>
                                            </div>

                                            <div class="col-md-12">
                                                <label>Select Product</label>
                                                {!! Form::select('product', \App\Product::where('entity_id', Auth::user()->entity_id)->pluck('product_name', 'id'), "",
                                                ['class' => 'select2 form-control', 'id'=>'product']) !!}
                                                <div style="color: red">{{$errors->first("product") }}</div>
                                            </div>

                                            <div class="col-md-12">
                                                <label>Proffered Location</label>
                                                <input type="text" name="location" required id="location-text-box" autofocus value="{{old('location')}}" class="form-control">
                                                <div style="color: red">{{$errors->first("location") }}</div>

                                            </div>


                                            <div class="col-md-12">

                                                <label for="unit_size">Unit Size</label>
                                                {!! Form::text('unit_size', '',['class'=>'form-control','id'=>'unit_size','placeholder'=>'eg. 1kg Rice', 'required']) !!}

                                            </div>


                                            <div class="col-md-12">
                                                <label for="units">Number of Units Wanted</label>
                                                {!! Form::number('units', '',['class'=>'form-control','id'=>'units','min'=>'0','placeholder'=>'eg. 50','required']) !!}

                                            </div>


                                            <div class="col-md-12">
                                                <label for="delivery">Delivery Method</label> <br>
                                                <input type="radio" name="delivery" checked value="1"> I need the goods/services delivered to me<br>
                                                <input type="radio" name="delivery" value="0"> I will collect the goods/services myself
                                            </div>

                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Terms (Please outline your terms and conditions)</label>
                                                    <textarea name="terms" class="form-control" required placeholder="" rows="6">{{old('terms')}}</textarea>
                                                    <div style="color: red">{{$errors->first("terms") }}</div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="column">
                                        <div class="col-sm-6 col-md-6 hidden-xs" style="height: 500px;width: 50%; ">

                                            <div id="mapcanvas"></div>

                                        </div>
                                    </div>


                                </div>

                                <div class="col-md-6" style="margin-top: 2%">
                                    <input type="submit" value="Create" class="btn btn-block btn-primary align-left">
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div>


        </div>

    </div>


    @section('css')
        <style>
            /* Always set the map height explicitly to define the size of the div
             * element that contains the map. */
            #mapcanvas {
                height: 100%;
                width: 100%;
            }

        </style>
    @endsection

    @section("scripts")
        <script>


            var map;
            var marker;
            var lat = document.getElementById('lat');
            var lng = document.getElementById('lng');



            function initMap() {

                map = new google.maps.Map(document.getElementById('mapcanvas'), {
                    //center: pyrmont,
                    zoom: 15
                });

                // Get GEOLOCATION
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        var pos = new google.maps.LatLng(position.coords.latitude,
                            position.coords.longitude);

                        map.setCenter(pos);
                        marker = new google.maps.Marker({
                            position: pos,
                            map: map,
                            draggable: false
                        });
                    }, function() {
                        handleNoGeolocation(true);
                    });
                } else {
                    // Browser doesn't support Geolocation
                    handleNoGeolocation(false);
                }


                function handleNoGeolocation(errorFlag) {
                    if (errorFlag) {
                        var content = 'Error: The Geolocation service failed.';
                    } else {
                        var content = 'Error: Your browser doesn\'t support geolocation.';
                    }

                    var options = {
                        map: map,
                        position: new google.maps.LatLng(0, 0),
                        content: content
                    };

                    map.setCenter(options.position);
                    marker = new google.maps.Marker({
                        position: options.position,
                        map: map,
                        draggable: false
                    });

                }


                // get places auto-complete when user type in location-text-box
                var input = /** @type {HTMLInputElement} */
                    (
                        document.getElementById('location-text-box'));


                var autocomplete = new google.maps.places.Autocomplete(input);
                autocomplete.bindTo('bounds', map);

                var infowindow = new google.maps.InfoWindow();
                marker = new google.maps.Marker({
                    map: map,
                    anchorPoint: new google.maps.Point(0, 0),
                    draggable: false
                });


                google.maps.event.addListener(autocomplete, 'place_changed', function() {
                    infowindow.close();
                    marker.setVisible(false);
                    var place = autocomplete.getPlace();
                    if (!place.geometry) {
                        return;
                    }

                    // If the place has a geometry, then present it on a map.
                    if (place.geometry.viewport) {
                        map.fitBounds(place.geometry.viewport);
                    } else {
                        map.setCenter(place.geometry.location);
                        map.setZoom(17); // Why 17? Because it looks good.
                    }

                    marker.setPosition(place.geometry.location);
                    marker.setVisible(true);

//                    console.log(place.geometry.location.lat());
//                    console.log(place.geometry.location.lng());
                    lat.value = place.geometry.location.lat();
                    lng.value = place.geometry.location.lng();


                    var address = '';
                    if (place.address_components) {
                        address = [
                            (place.address_components[0] && place.address_components[0].short_name || ''), (place.address_components[1] && place.address_components[1].short_name || ''), (place.address_components[2] && place.address_components[2].short_name || '')
                        ].join(' ');
                    }

                });


            }





        </script>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA50nWlMzxA5OEFPJLoba4oAGvGI40k6Jc&libraries=places&callback=initMap" async defer></script>

    @endsection



