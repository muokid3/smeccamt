


    <div class="container-fluid">
        <div class="row">

                <h3 class="text-center">
                    {{Auth::user()->bus_name}}
                </h3>
                <h5 class="text-center">You are registered as on SMECCA as: {{Auth::user()->type->name}} </h5>
            <hr/>

            <div class="container-fluid">


                <div class="col-md-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-6">
                                    <i class="fa fa-usd fa-5x"></i>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <p class="announcement-heading">{{\App\Donation::select('donations.id')
                                                                            ->join('donor_requests', 'donations.request_id', '=', 'donor_requests.id')
                                                                            ->where('donor_requests.entity_id', '=', Auth::user()->entity_id)
                                                                            ->where('donations.status', '=', 2)
                                                                            ->count()}}</p>
                                    <p class="announcement-text"> Donations</p>
                                </div>
                            </div>
                        </div>
                        <a href="{{url('profile/donors')}}">
                            <div class="panel-footer announcement-bottom">
                                <div class="row">
                                    <div class="col-xs-6">
                                        View Donors
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>


                <div class="col-md-3">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-6">
                                    <i class="fa fa-money fa-5x"></i>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <p class="announcement-heading">

                                        {{\App\Donation::select('donations.id')->join('donor_requests', 'donations.request_id', '=', 'donor_requests.id')
                                                                            ->where('donor_requests.entity_id', '=', Auth::user()->entity_id)
                                                                            ->where('donations.status', '=', 1)
                                                                            ->count()}}

                                    </p>
                                    <p class="announcement-text">Donor Offers <i class=""></i>  </p>
                                </div>
                            </div>
                        </div>
                        <a href="{{url('profile/donor_requests')}}">
                            <div class="panel-footer announcement-bottom">
                                <div class="row">
                                    <div class="col-xs-6">
                                        View Offers
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>



                <div class="col-md-3">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-6">
                                    <i class="fa fa-truck fa-5x"></i>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <p class="announcement-heading">
                                        {{

                                         \Illuminate\Support\Facades\DB::table('bids')
                                            ->select('bids.bidder_entity_id as bidder_entity_id')
                                            ->join('purchase_orders', 'bids.purchase_order_id', '=', 'purchase_orders.id')
                                            ->where('purchase_orders.entity_id','=',Auth::user()->entity_id)
                                            ->where('bids.status','=',2)
                                            ->distinct('bidder_entity_id')
                                            ->count('bidder_entity_id')
                                        }}
                                    </p>
                                    <p class="announcement-text">Suppliers</p>
                                </div>
                            </div>
                        </div>
                        <a href="{{url('profile/suppliers')}}">
                            <div class="panel-footer announcement-bottom">
                                <div class="row">
                                    <div class="col-xs-6">
                                        View
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>



                <div class="col-md-3">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-6">
                                    <i class="fa fa-line-chart fa-5x"></i>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <p class="announcement-heading">{{\App\PurchaseOrder::where('status',1)->where('entity_id','=',Auth::user()->entity_id)->count()}}</p>
                                    <p class="announcement-text">My Orders</p>
                                </div>
                            </div>
                        </div>
                        <a href="{{url('profile/purchase_orders')}}">
                            <div class="panel-footer announcement-bottom">
                                <div class="row">
                                    <div class="col-xs-6">
                                        Details
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>



                <div class="col-md-6">

                    <h3>Latest Supply bids</h3>

                    <table class="table table-condensed table-stripped table-bordered table-hover small">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Product</th>
                            <th>Order</th>
                            <th>Bidder</th>
                            <th>Bid Amount</th>
                        </tr>
                        </thead>
                        <tbody>


                        @foreach($bids as $bid)
                            <tr>
                                <td>
                                    <a href="{{url('profile/purchase_orders/'.$bid->purchase_order_id)}}"> <i class="fa fa-eye"></i></a>
                                </td>
                                <td>
                                    {{\App\Product::find($bid->product_id)->product_name}}
                                </td>
                                <td>
                                    {{$bid->unit_size}} - {{$bid->units}}
                                </td>
                                <td>
                                    {{\App\Entity::find($bid->bidder_entity_id)->name}}
                                </td>
                                <td>
                                    {{$bid->total_price}}
                                </td>
                            </tr>

                        @endforeach

                        </tbody>

                    </table>

                    @if(sizeof($bids) == 0)
                    <i>Data will appear here once you start transacting</i>
                    @endif


                </div>

                {{--<div class="col-md-6">--}}
                    {{--<div id='grafico1' class="grafico"></div>--}}
                {{--</div>--}}

                {{--<div class="col-md-6">--}}
                    {{--<div id='grafico2' class="grafico"></div>--}}
                {{--</div>--}}
                {{--<div class="col-md-6">--}}
                    {{--<div id='grafico3' class="grafico"></div>--}}
                {{--</div>--}}




                <div class="col-md-6">

                    <h3>New Messages</h3>

                    <table class="table table-condensed table-bordered table-hover small">
                        <thead>
                        <tr>
                            <th style="width:30px;"></th>
                            <th>From</th>
                            <th>Subject</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>




                        {{--<tr class="text-info">--}}
                            {{--<td>--}}
                                {{--<i class="fa fa-info"></i>--}}
                            {{--</td>--}}
                            {{--<td>--}}
                                {{--Mr Joe--}}
                            {{--</td>--}}
                            {{--<td>--}}
                                {{--Congrats--}}
                            {{--</td>--}}
                            {{--<td>--}}
                                {{--<a href="#"> <i class="fa fa-eye"></i></a>--}}
                            {{--</td>--}}

                        {{--</tr>--}}



                        </tbody>

                    </table>

                    <i>Data will appear here once you start transacting</i>


                </div>




            </div>


        </div>
    </div>




    @section('scripts')
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/highcharts-more.js"></script>

        <script>

            document.addEventListener("DOMContentLoaded", function(event) {

                Highcharts.chart('grafico1', {

                    credits: {
                        enabled: false
                    },
                    title: {
                        text: 'Revenue by brand'
                    },


                    yAxis: {
                        title: {
                            text: ' $'
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },

                    plotOptions: {
                        series: {
                            pointStart: 1
                        }
                    },

                    series: [{
                        name: 'Brand 1',
                        data: [23, 27, 32, 26, 25, 27, 22, 19]
                    }, {
                        name: 'Brand 2',
                        data: [45, 49, 43, 41, 42, 41, 38, 34]
                    }, {
                        name: 'Brand 3',
                        data: [43, 58, 51, 52, 51, 60, 62, 65]
                    }, {
                        name: 'Brand 4',
                        data: [null, null, 23, 15, 19, 21, 27, 30]
                    }]

                });









                Highcharts.chart('grafico2', {
                    chart: {
                        type: 'areaspline'
                    },

                    credits: {
                        enabled: false
                    },
                    title: {
                        text: 'Forecast - ROI'
                    },


                    yAxis: {
                        title: {
                            text: 'Months'
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },

                    plotOptions: {
                        series: {
                            pointStart: 1
                        }
                    },

                    series: [{
                        name: 'Product 1',
                        data: [15, 27, 32, 26, 25, 20]
                    }, {
                        name: 'Product 2',
                        data: [43, 41, 42, 41, 38, 34]
                    }, {
                        name: 'Product 3',
                        data: [51, 52, 51, 60, 62, 65]
                    }, {
                        name: 'Product 4',
                        data: [null, null, 29, 22, 19, 21]
                    }]

                });




                Highcharts.chart('grafico3', {
                    chart: {
                        type: 'column'
                    },

                    credits: {
                        enabled: false
                    },
                    title: {
                        text: 'Runway overview'
                    },
                    xAxis: {
                        categories: ['Product 1', 'Product 2', 'Product 3', 'Product 4']
                    },
                    yAxis: {
                        title: {
                            text: 'value (USD)'
                        },
                        stackLabels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold',
                                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                            }
                        }
                    },
                    legend: {
                        align: 'center',
                        verticalAlign: 'bottom',
                        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                        borderColor: '#CCC',
                        shadow: false
                    },
                    tooltip: {
                        headerFormat: '<b>{point.x}</b><br/>',
                        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
                    },
                    plotOptions: {
                        column: {
                            stacking: 'normal',
                            dataLabels: {
                                enabled: false,
                                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                            }
                        }
                    },
                    series: [{
                        name: 'Debit',
                        data: [0,0,0,15],
                        color : '#FA5858'

                    }, {
                        name: 'Available',
                        data: [150, 130, 25,  0],
                        color : '#58FA58'
                    }, {
                        name: 'Used',
                        data: [100, 140, 100, 205],
                        color : '#FE9A2E'

                    }]
                });




            });

        </script>
    @endsection

    @section('css')

        <style>
            .grafico {
                min-width: 310px;
                max-width: 400px;
                height: 280px;
                margin: 0 auto
            }

            .main-header {
                font-size: x-large;
                color : #888;
                font-family: Verdana;
                margin-bottom: 20px;
            }

            .destaque {
                color: #f88;
                font-weight: bolder;
            }

            .highcharts-tooltip h3 {
                margin: 0.3em 0;
            }
        </style>
    @endsection

