


    <div class="container-fluid">
        <div class="row">

                <h3 class="text-center">
                    {{Auth::user()->bus_name}}
                </h3>
                <h5 class="text-center">Here is a list of Donors that you have previously intereacted/transacted with</h5>
            <hr/>

            <table class="table table-striped table-responsive">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Industry</th>
                    <th>Location</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                @foreach($donors as $donor)
                    <tr>
                        <td>{{$donor->name}}</td>
                        <td>
                            {{$donor->industry->industry}}
                        </td>

                        <td>
                            {{$donor->location}}
                        </td>

                        <td>
                            <a href="{{url('/profile/donors/'.$donor->id)}}">
                                <i class="fa fa-eye"></i> View Donor
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {{$donors->links()}}

        </div>

    </div>

