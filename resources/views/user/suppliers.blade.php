


    <div class="container-fluid">
        <div class="row">

                <h3 class="text-center">
                    {{Auth::user()->bus_name}}
                </h3>
                <h5 class="text-center">A list of suppliers you have previously worked with </h5>
            <hr/>

            <table class="table table-striped table-responsive">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Industry</th>
                    <th>Location</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                @foreach($suppliers as $supplier)
                    <tr>
                        <td>{{$supplier->name}}</td>
                        <td>
                           {{$supplier->industry->industry}}
                        </td>

                        <td>
                           {{$supplier->location}}
                        </td>

                        <td>
                            <a href="{{url('/profile/suppliers/'.$supplier->id)}}">
                                <i class="fa fa-eye"></i> View Supplier
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {{$suppliers->links()}}

        </div>

    </div>

