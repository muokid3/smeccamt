<?php $page_title = "Home"; ?>
@extends('layouts.app')

@section('content')
    <div class="main main-unraised" role="main">
        <div class="section">
            <div class="row">
                <div class="col-md-4 col-md-offset-4 col-sm-12">
                    <h2 class="text-center">
                        Welcome Back to SMECCA!
                    </h2>
                    <h3 class="text-center">Enter your credentials to sign in</h3>
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-12 {{ $errors->has('email') ? ' has-error' : '' }}">
                                <label>Email Address</label>
                                <div class="input-group margin-20">
                                    <span class="input-group-addon">
                                        <i class="fa fa-envelope"></i>
                                    </span>
                                    <input id="email" type="text" class="form-control"
                                           name="email"
                                           value="{{ old('email') }}" autocomplete="off"
                                           placeholder="Email Address" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block"> <strong>{{ $errors->first('email') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label>Password</label>
                                <div class="input-group margin-20">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i> </span>
                                    <input id="password" type="password" class="form-control"
                                           name="password" placeholder="Password"
                                           autocomplete="off"
                                           required>

                                    @if ($errors->has('password'))
                                        <span class="help-block"> <strong>{{ $errors->first('password') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <a class="btn btn-link" href="{{url('/password/reset')}}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                        <input type="submit" class="btn btn-primary btn-lg btn-block"
                               value="Sign In Now">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <a class="btn btn-link" href="{{url('/register')}}"> Create an account now</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="section section-primary"></div>
    </div>
@endsection
