<?php $page_title = "Enter Email"; ?>
@extends('layouts.app')

<!-- Main Content -->
@section('content')
    <div class="main" role="main">
        <div id="content" class="content full">
            <div class="container">
                <header class="section-header underline">
                    <h2 class="text-center">Reset Password</h2>
                    <h4 class="text-center">Enter your email to reset password</h4>
                </header>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email">E-Mail Address</label>

                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Send Password Reset Link
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
