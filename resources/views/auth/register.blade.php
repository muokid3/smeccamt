<?php $page_title = "Enroll"; ?>
@extends('layouts.app')

@section('content')
    <div class="main main-unraised">
        <div class="section">
            <div class="row">

                <div class=" col-md-6 col-lg-6 col-sm-12 col-xs-12">
                    <div class="widget sidebar-widget">

                        <h2 style="margin-top: 20px; text-align: center">You are signing up as:</h2>

                        <div id="spinning" class="hidden" style="text-align: center; margin-top: 25%">
                            <i style="font-size:6em; color:Tomato" class="fa fa-spin fa-spinner"></i>
                        </div>

                        <h3 id="group_name" style="margin-top: 10%; margin-bottom: 5%;color: #e74c3c!important" class="text-center">  </h3>
                        <p id="group_desc">
                        </p>

                    </div>
                </div>


                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                    <div class="widget sidebar-widget">
                        <h2 class="text-center">
                            Let's get started!
                        </h2>

                        <h3 class="text-center">Enter Details to Enroll</h3><br>
                        <p style="color: #e74c3c!important" class="text-center"><strong>Tell us a little about yourself</strong></p>
                        <div class="alert alert-danger" id="error"></div>
                        <form action="{{url('/register')}}" method="post">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-6 {{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label>Name<span class="text-danger">*</span> </label>
                                    <input type="text" required id="name" name="name" class="form-control"
                                           value="{{ old('name') }}"
                                           placeholder="Your name*">
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                    @endif
                                </div>

                                <div class="col-md-6 {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label>Email Address <span class="text-danger">*</span> </label>
                                    <input type="email" required name="email" value="{{ old('email') }}" class="form-control"
                                           placeholder="Your email*">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <label>Business Name<span class="text-danger">*</span> </label>
                                    <input type="text" id="bus_name" required placeholder="Your Business Name.*" value="{{ old('bus_name') }}"
                                           name="bus_name"
                                           class="form-control"/>
                                    @if ($errors->has('bus_name'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('bus_name') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>

                            <div id="after_val" >
                                <div class="row">

                                    <div class="col-md-6 col-sm-12 {{ $errors->has('user_group') ? ' has-error' : '' }}">
                                        <label for="user_group">Sign up as <span class="text-danger">*</span> </label>


                                            {!! Form::select('user_group', \App\UserGroup::pluck('name', 'id'), null,
                                            ['class' => 'select2 form-control', 'id'=>'user_group','required']) !!}
                                            {{$errors->first("user_group") }}

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 {{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label>Password <span class="text-danger">*</span> </label>
                                        <input type="password" required name="password" value="{{ old('password') }}"
                                               class="form-control" placeholder="Password">
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-md-6 {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                        <label>Confirm Password<span class="text-danger">*</span> </label>
                                        <input type="password" required name="password_confirmation"
                                               value="{{ old('password_confirmation') }}" class="form-control"
                                               placeholder="Confirm password">
                                        @if ($errors->has('password_confirmation'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <br/>
                                <input type="submit" class="btn btn-primary btn-block" value="Enroll">
                            </div>
                        </form>
                    </div>
                </div>


            </div>
        </div>
        <div class="section section-primary">
        </div>
    </div>
@endsection
@section("script")
    <script>

        $(document).ready(function (e) {


            var user_group = $("#user_group");
            var group_name = $("#group_name");
            var group_desc = $("#group_desc");
            var spinning = $("#spinning");


            group_name.html("");
            group_name.addClass("hidden");

            group_desc.html("");
            group_desc.addClass("hidden");

            spinning.removeClass("hidden");


            $.ajax("{{url('/get_user_group')}}/" + user_group.val(), {
                success: function (message) {
                    console.log(message);
                    var temp = JSON.parse(message);
                    var name = temp.name;
                    var description = temp.description;

                    group_name.html(name);
                    group_desc.html(description);

                    spinning.addClass("hidden");
                    group_name.removeClass("hidden");
                    group_desc.removeClass("hidden");

                },
                error: function (error) {
                    console.log(error);
                }
            });

            user_group.on('change', function () {

                console.log(user_group.val());
                group_name.html("");
                group_name.addClass("hidden");

                group_desc.html("");
                group_desc.addClass("hidden");

                spinning.removeClass("hidden");

                $.ajax("{{url('/get_user_group')}}/" + user_group.val(), {
                    success: function (message) {
                        console.log(message);
                        var temp = JSON.parse(message);
                        var name = temp.name;
                        var description = temp.description;

                        group_name.html(name);
                        group_desc.html(description);

                        spinning.addClass("hidden");
                        group_name.removeClass("hidden");
                        group_desc.removeClass("hidden");

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            });


            var pxkd098 = $("#validate");
            var xone = $("#fname");
            var error = $("#error");
            var dob = $("#dob");
            var v = $("#v");
            var hidden = $("#after_val");
            var gender = $("#gender");
            var xonetwo = $("#lname");
            var pinto = $("#id_no");
            error.hide();
            pxkd098.on("click", function (e) {
                error.html("Processing ...");
                error.slideDown();
                $.ajax({
                    type: "POST",
                    url: "{!! url('check_id') !!}",
                    data: {id_no: pinto.val(), fname: xone.val(), lname: xonetwo.val()},
                    timeout: 2000,
                    success: function (response) {
                        response = JSON.parse(response);
                        if (response.error) {
                            error.html(response.message);
                            error.slideDown();
                        } else {
                            gender.val(response.gender == "M" ? "MALE" : "FEMALE");
                            dob.val(response.dob);
                            v.val(response.v);
                            pxkd098.addClass("hidden");
                            hidden.removeClass("hidden");
                            error.hide();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
//                        alert(errorThrown);
//                        if(textStatus==="timeout") {
                        gender.val("Male");
                        v.val(0);
                        pxkd098.addClass("hidden");
                        hidden.removeClass("hidden");
                        error.hide();
//                        }
                    }
                });

            });
        });
    </script>
@endsection