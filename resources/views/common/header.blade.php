<nav class="navbar navbar-fixed-top navbar-toggleable-sm" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="{{url('/')}}">
                <img class="logo-icon " src="{{url('/images/logo.png')}}">
            </a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{url('/')}}">Home</a></li>

                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">About Us <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">About SMECCA</a></li>
                        <li><a href="#">Monitoring Tool</a></li>
                    </ul>
                </li>

                <li><a href="#">FAQ</a></li>


                <li><a href="{{url('/contact')}}">Contact US</a></li>
                @if(Auth::check())

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Profile <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{url('/profile')}}"><span class="fa fa-user"></span> Profile</a></li>
                            <li><a href="{{url('/logout')}}"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                        </ul>
                    </li>
                @else

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">My smecca Account<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{url('/register')}}">Enroll</a></li>
                            <li><a href="{{url('/login')}}">Login</a></li>
                        </ul>
                    </li>

                @endif
            </ul>
        </div>
    </div>
</nav>

{{--@if(Session::has('message'))--}}
{{--<div class="alert alert-info">{{Session::get('message')}}</div>--}}
{{--@endif--}}
{{--@if(Session::has('error'))--}}
{{--<div class="alert alert-danger">{{Session::get('error')}}</div>--}}
{{--@endif--}}

