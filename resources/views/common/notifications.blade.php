@if (session()->has('flash_notification.message'))
    <div class="msg_alerts text-center m-b-0 alert alert-{{ session('flash_notification.level') }}">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {!! session('flash_notification.message') !!}
    </div>
@endif
