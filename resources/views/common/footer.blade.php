<!-- Start site footer -->
<footer class="site-footer">
    <div class="row">
            <div class="row">
                <h4>Disclaimer</h4>
                <p class="text-black">
                    The content of this website is provided for information purposes only. No claim is made to the accuracy or authenticity of the content. Links provided to third party websites
                    do not constitute... <a href="#">Read <u>Full Disclaimer</u></a>
                </p>
            </div>
            <div class="row">
                <div class="col-md-3 col-xs-6 col-sm-3">
                    <h4>
                        Quick Links
                    </h4>
                    <hr/>
                    <ul>
                        <li><a href="#">FAQs</a></li>
                        <li><a href="#">Disclaimer</a></li>
                        <li><a href="#">Aboput SMECCA</a></li>
                        <li><a href="#">Monitoring</a></li>
                        <li><a href="#">Contact Us</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-3">
                    <h4> Address</h4>
                    <hr/>
                    <ul class="text-black">
                        <li class="">Address: 15th Floor, KCB Towers</li>
                        <li class="">Town: Nairobi, Kenya</li>
                        <li class="">Phone: +254 20 000 000</li>
                        <li class="">Email: <a href="mailto:info@metropol.co.kee">info@metropol.co.ke</a></li>
                        <li class="">Hours: Open today · 8AM–5PM</li>
                    </ul>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-3">
                    <h4>
                        Quick Links
                    </h4>
                    <hr/>
                    <ul>
                        <li><a href="#">FAQs</a></li>
                        <li><a href="#">Disclaimer</a></li>
                        <li><a href="#">Aboput SMECCA</a></li>
                        <li><a href="#">Monitoring</a></li>
                        <li><a href="#">Contact Us</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-3">
                    <h4> Address</h4>
                    <hr/>
                    <ul class="text-black">
                        <li class="">Address: 15th Floor, KCB Towers</li>
                        <li class="">Town: Nairobi, Kenya</li>
                        <li class="">Phone: +254 20 000 000</li>
                        <li class="">Email: <a href="mailto:info@metropol.co.kee">info@metropol.co.ke</a></li>
                        <li class="">Hours: Open today · 8AM–5PM</li>
                    </ul>
                </div>
            </div>

    </div>
    <div class="row">
        <div class="col-sm-12 text-small text-center">
            <p>A project of Metropol Corporation. &copy All right Reversed.</p>
        </div>
    </div>
</footer>
<!-- End site footer -->


<a id="back-to-top"><i class="fa fa-angle-double-up"></i></a>
