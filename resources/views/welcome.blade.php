<?php $page_title = "Home"; ?>
@extends('layouts.app')

@section('content')
    <div class="index-header" >
        <div class="container">
            <div class="row">

                <div style="font-size: 1.5em; text-align: center"
                     class="landing-header hidden-lg hidden-md col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1">
                    SMECCA MONITORING TOOL
                </div>
                <div class="hidden-lg hidden-md col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2">
                    <a style="margin-top: 10px;" href="{{url('/register')}}"
                       class="btn btn-lg btn-block btn-danger">
                        <i class="fa fa-pencil"></i> Enroll Now</a>


                    <a style="margin-top: 10px;" href="{{url('/login')}}" class="btn btn-lg btn-block btn-primary">
                        <i class="fa fa-sign-in"></i> Sign In Now</a>
                </div>
                <div class="col-md-9 hidden-xs hidden-sm">

                    <div class="brand">
                        <div class="landing-header">SMECCA MONITORING TOOL</div>
                        <br/>
                        <p style="margin-right: 10%">
                            Provides visibility on your transactions as you engage in economic activities with your customers.<br>
                            Would you like to monitor performance of your investments? Then SMECAA monitoring tool is the way to go
                        </p>


                        @if(@Auth::guest())
                            <div class="col-md-12">
                                <a href="{{url('/register')}}" class="btn btn-lg btn-danger">
                                    <i class="fa fa-pencil"></i> Enroll Now</a>

                            </div>
                        @else
                            <div class="col-md-12">
                                <a href="{{url('/profile')}}" class="btn btn-lg btn-danger">
                                    <i class="fa fa-user"></i> Go To my Profile</a>

                            </div>
                        @endif
                    </div>
                </div>
                <div class="hidden-sm col-md-3 hidden-xs">
                    @if(@Auth::guest())
                        <h3 class="widgettitle text-white">Sign in here</h3>
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-12 {{ $errors->has('email') ? ' has-error' : '' }}">

                                    @if ($errors->has('email'))
                                        <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                                    @endif
                                    <label class="text-white">Email address</label>
                                    <div class="input-group margin-20">
                                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                        <input id="email" type="text" class="form-control"
                                               name="email"
                                               value="{{ old('email') }}" autocomplete="off"
                                               placeholder="Email Address" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label class="text-white">Password</label>
                                    <div class="input-group margin-20">
                                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                        <input id="password" type="password" class="form-control"
                                               name="password" placeholder="Password"
                                               autocomplete="off"
                                               required>

                                    </div>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                            <br/>
                            <input type="submit" class="btn btn-primary btn-block"
                                   value="Sign In Now">
                            <div class="row">
                                <div class="col-md-6">
                                    <a class="btn btn-link text-white" href="{{url('/password/reset')}}">
                                        Forgot Your Password?
                                    </a>
                                </div>
                            </div>
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- End Hero Slider -->


    <!-- Lead Content -->
    <div class="row">
        <div class="main main-raised ">
            <div class="section">
                <div class="row">

                    <div class="col-md-6  col-sm-6 col-xs-6">
                        <h3 class="strong">Welcome to SMECCA </h3>
                        <ul class="list-group">
                            <li>Do you buy goods on credit?</li>
                            <li>Do you sale goods on Credit?</li>
                            <li>Do you give credit facilities to your customers?</li>
                            <li>Do you donate funds to start ups?</li>
                        </ul>
                    </div>


                    <div class="col-md-6  col-sm-6 col-xs-6">
                        <h3 class="strong">This is a platform where </h3>
                        <ul class="list-group">
                            <li>Customers get connected to supplier / sellers</li>
                            <li>Banks exchange economic value with their customers</li>
                            <li>Donors – donate and monitor usage of the funds</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="section section-cards section-primary">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h3 class="title text-white strong"><strong>Is SMECCA For You?</strong></h3>
                        <p class="text-white">
                            Would you like to monitor performance of your investments? Then SMECAA monitoring tool is the way to go
                        </p>
                        <div class="spacer-50"></div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="very-latest-post format-standard">
                                    <h3 class="text-white title">About SMECCA</h3>
                                    <p class="text-white text-left">
                                        Is a business management tool that enables SME’s organize their work and get a rating on their transactions
                                        which would enable them access affordable credit facility at better payment terms.

                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="very-latest-post format-standard">
                                    <h3 class=" text-white title">SMECCA in Kenya</h3>
                                    <p class="text-white text-left">
                                        The number one solution for SME’s across the region. Don’t be left out enroll now.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section">
                <div class="row">
                    <div class="col-md-4 hidden-sm hidden-xs">
                        <section class="upcoming-event format-standard event-list-item event-dynamic">
                            <a href="#" class="media-box">
                                <img width="350" src="{{url('/images/monitoring.jpg')}}" alt="">
                            </a>
                            <div class="upcoming-event-content">
                                <h3>On The Focus</h3>
                                <h2><a href="#" class="event-title">SMECCA</a></h2>
                            </div>
                        </section>
                    </div>
                    <div class="col-md-8 col-sm-7">
                        <div class="element-block events-listing">
                            <div class="events-listing-header">
                                <h3 class="element-title">Components</h3>
                                <hr class="sm">
                            </div>
                            <div class="events-listing-content">
                                <div class="event-list-item event-dynamic">
                                    <div class="event-list-item-date">
                                    	<h4 class="event-date">
                                        	<span class="event-day"><i class="fa fa-search-plus"></i></span>
                                            <b class="event-title">Monitoring</b>

                                        </h4>
                                    </div>
                                    <div class="event-list-item-info">
                                        <div class="lined-info">
                                            <ul class="list-group">
                                                <li>Donors – don’t just give funds – monitor them</li>
                                                <li>Credit providers – Have a 360 degrees view of your customers and lend based on insights</li>
                                                <li>Do you give credit facilities to your customers?</li>
                                                <li>Supplier – Track movement of your goods and monitor payments. Only deliver on demand.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="event-list-item event-dynamic">
                                    <div class="event-list-item-date">
                                    	<h4 class="event-date">
                                        	<span class="event-day"><i class="fa fa-exchange"></i></span>
                                                <b class="event-title">Trading</b>
                                        </h4>
                                    </div>
                                    <div class="event-list-item-info">
                                        <div class="lined-info">
                                            <p class="strong">This is a platform where </p>
                                            <ul class="list-group">
                                                <li>Customers get connected to supplier / sellers</li>
                                                <li>Banks exchange economic value with their customers</li>
                                                <li>Donors – donate and monitor usage of the funds</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section section-cards section-primary">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="very-latest-post format-standard">
                            <h3 class="strong text-white">Trade And Exchange</h3>
                            <hr class="sm">
                            <p class="text-white">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </p>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="very-latest-post format-standard">
                                    <h3 class="title text-white">Get Donors</h3>
                                    <p class="text-white">
                                        Do you have a project that needs to be funded, then enroll and get funds.
                                    </p>
                                    <p><a href="{{url('/register')}}" class="text-white description">Start now</a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="very-latest-post format-standard">
                                    <h3 class="title text-white">Improve Your Competency</h3>
                                    <p class="text-white">
                                        You need credit facility to enable you realize your into your dreams, your score is your liberator. Get it here…

                                    </p>
                                    <p><a href="{{url('/register')}}" class="text-white description">Enroll Now...</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
