<?php $page_title = "Contact us"; ?>
@extends('layouts.app')

@section('content')
    <div class="body">

        <!-- Start Body Content -->
        <div class="main main-unraised" role="main">
            <div id="content" class="content full" style="margin-top: 20px; padding-bottom: 50px; padding-top: 50px">
                <div class="container">

                    <div class="lgray-bg breadcrumb-cont">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/')}}">Home</a></li>
                            <li class="active">{{$page_title}}</li>
                        </ol>
                        <hr/>
                    </div>

                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <h2>Our Locations</h2>
                            <hr class="sm">
                            <h4 class="short accent-color">Physical Address</h4>
                            <p> 15th Floor, KCB Towers,<br> Elgon Road, Nairobi<br>
                                <a href="mailto:info@metropol.co.kee">info@metropol.co.ke</a>
                            </p>
                            <hr class="fw cont">
                            <h4 class="short accent-color">Telephone</h4>
                            <p> Phone: +254 20 111 111 <br>
                            </p>
                            <hr class="fw cont">
                            <h4 class="short accent-color">Office Hours</h4>
                            <p> Mon - Fri <br>0800hrs – 1700hrs<br>
                            </p>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            @if(Session::has('message'))
                                <div class="alert alert-info">{{Session::get('message')}}</div>
                            @elseif(Session::has('success'))
                                <div class="alert alert-success">{{Session::get('success')}}</div>
                            @elseif(Session::has('error'))
                                <div class="alert alert-danger">{{Session::get('error')}}</div>
                            @endif

                            <form method="POST" id="contactform" name="contactform" class="form-horizontal"
                                  action="{{ url('/sendemail') }}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-12">

                                            <input type="text" id="fname" name="fname" required="required"
                                                   class="form-control input-lg" placeholder="First name*"
                                            value="{{Auth::user() ? ''.Auth::user()->first_name.'' : ''}}">
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" id="lname" required="required" name="lname"
                                                   class="form-control input-lg" placeholder="Last name"
                                                   value="{{Auth::user() ? ''.Auth::user()->last_name.'' : ''}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="email" id="email" required="required" name="email" class="form-control input-lg"
                                                   placeholder="Email*" value="{{Auth::user() ? ''.Auth::user()->email.'' : ''}}">
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="number" id="phone" required="required" name="phone" class="form-control input-lg"
                                                   placeholder="Phone" value="{{Auth::user() ? ''.Auth::user()->phone_no.'' : ''}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" id="subject" required="required" name="subject" class="form-control input-lg"
                                                   placeholder="Subject*">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <textarea cols="6" rows="7" id="comments" name="message" class="form-control input-lg" placeholder="Message"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <input id="submit" name="submit" type="submit"
                                               class="btn btn-primary btn-lg btn-block" value="Send now!">
                                    </div>
                                </div>
                            </form>
                            <div class="clearfix"></div>
                            <div id="message"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Body Content -->

    </div>
@endsection
